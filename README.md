# inx-utils

A set of inkscape extensions to provide small features to create or
manipulate objects.


## Basic Usage

TBD.


## Installation

Copy the files in the `src/` directory into the user extensions
directory (see 'Inkscape Preferences > System' for the exact location)
and relaunch Inkscape.

### The extensions will be available as:

**Extensions > Util:**
- Arrange Objects
- Copy Objects
- Draw BBox
- Flip Horizontal
- Flip Vertical
- Groups and Layers
- Mirror Objects
- Remove Attributes
- Remove Overlaps
- Reset Ids
- Rotate 90° CW
- Rotate 90° CCW
- Scale Objects
- Stretch Objects
- Toggle Background
- Transform Objects
- Transform Polar
- Transformation Center


## References

Related feature requests:
* [lp171855](https://bugs.launchpad.net/inkscape/+bug/171855): "Duplicate with transform"
* [lp170677](https://bugs.launchpad.net/inkscape/+bug/170677): "add button - apply to copy"
* [lp1206420](https://bugs.launchpad.net/inkscape/+bug/1206420): "Transformation with 'apply to clone' and 'apply to clone' checkboxes"
* [lp171882](https://bugs.launchpad.net/inkscape/+bug/171882): "individual flipping of several objects"
* [lp1480646](https://bugs.launchpad.net/inkscape/+bug/1480646): "Rotate Selection toolbar icons rotate individually"
* [lp171797](https://bugs.launchpad.net/inkscape/+bug/171797): "allow to mirror objects at any axis"
* [lp378865](https://bugs.launchpad.net/inkscape/+bug/378865): "expand a group without changing sizes of group members (sparse group)"
* [lp1629688](https://bugs.launchpad.net/inkscape/+bug/1629688): "move objects in radial direction, without scaling"
* [lp170947](https://bugs.launchpad.net/inkscape/+bug/170947): "Alignment with resizing"
* [lp569096](https://bugs.launchpad.net/inkscape/+bug/569096): "distribute" does not honor "relative to"
* [lp767822](https://bugs.launchpad.net/inkscape/+bug/767822): Distribute functions in the align/distribute dialog ignore "relative to" setting
* [lp569137](https://bugs.launchpad.net/inkscape/+bug/569137): Aligning objects centered is missing options
* [lp362360](https://bugs.launchpad.net/inkscape/+bug/362360): Two way Align Buttons
* [lp168788](https://bugs.launchpad.net/inkscape/+bug/168788): Aligning an group by aligning a member to a separate object
* [lp200720](https://bugs.launchpad.net/inkscape/+bug/200720): Set transform center numerically


## Background

Some of these extensions could be considered as prototype implementations of
features better added to core inkscape (without the many limitations of
script-based extensions).


## Source

The extensions are developed and maintained in:  
https://gitlab.com/su-v/inx-utils


## License

GPL-2+
