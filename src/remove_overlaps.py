#!/usr/bin/env python
"""
remove_overlaps - arrange objects by removing horizontal or vertical overlaps

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors
# pylint: disable=invalid-name

# standard libraries
import csv
import locale
import sys
from subprocess import Popen, PIPE

# inkscape library
try:
    import inkex_local as inkex
except ImportError:
    import inkex
import cubicsuperpath
import simpletransform

# local library
from transform_objects import EffectCompat, invertTransform


__version__ = '0.0'


ENCODING = sys.stdin.encoding
if ENCODING == 'cp0' or ENCODING is None:
    ENCODING = locale.getpreferredencoding()


# ---- util

def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    """Test approximate equality.

    ref:
        PEP 485 -- A Function for testing approximate equality
        https://www.python.org/dev/peps/pep-0485/#proposed-implementation
    """
    # pylint: disable=invalid-name
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


# ---- query objects

def run_shell(command_format, stdin_str=None, verbose=False):
    """Run command"""
    if verbose:
        inkex.debug(command_format)
    out = err = None
    myproc = Popen(command_format, shell=False,
                   stdin=PIPE, stdout=PIPE, stderr=PIPE)
    if sys.version_info > (2,):
        if stdin_str is not None:
            stdin_str = stdin_str.encode()
    out, err = myproc.communicate(stdin_str)
    if sys.version_info > (2,):
        if out is not None:
            out = out.decode(ENCODING)
    if myproc.returncode == 0:
        return out
    elif err is not None:
        inkex.errormsg(err)


def query_bbox(svg_file, obj_id, scale=1.0):
    """Return position, size of visual bbox of element with id *obj_id*.

    The values can be scaled by a single factor (document scale), and
    represent x, y and width, height relative to the coordinate system
    of the document's initial viewport.
    """
    opts = ['inkscape', '--shell']
    stdin_str = ""
    for arg in ['x', 'y', 'width', 'height']:
        stdin_str += '--file="{}" '.format(svg_file)
        stdin_str += '--query-id={} '.format(obj_id)
        stdin_str += '--query-{} '.format(arg)
        stdin_str += '\n'
    stdout_str = run_shell(opts, stdin_str, verbose=False)
    if stdout_str is not None:
        stdout_lst = stdout_str.split('>')
        if len(stdout_lst) >= 5:
            return [scale * float(s) for s in stdout_lst[1:5]]


# ----- process external command, files

def run_query(command_format, stdin_str=None, verbose=False):
    """Run command"""
    if verbose:
        inkex.debug(command_format)
    out = err = None
    myproc = Popen(command_format, shell=False,
                   stdin=PIPE, stdout=PIPE, stderr=PIPE,
                   universal_newlines=True)
    out, err = myproc.communicate(stdin_str)
    if myproc.returncode == 0:
        return out
    elif err is not None:
        inkex.errormsg(err)


def query_all(svgfile, id_list, scale=1.0, offset=None):
    """Spawn external inkscape to query all objects in *svgfile*."""
    opts = ['inkscape']
    opts.append('--query-all')
    opts.append(svgfile)
    stdout_str = run_query(opts, stdin_str=None, verbose=False)
    reader = csv.reader(stdout_str.splitlines())
    positions = {}
    for line in reader:
        if len(line) > 0 and line[0] in id_list:
            positions[line[0]] = [scale * float(v) for v in line[1:]]
            if offset is not None and len(offset) == 2:
                for i in range(2):
                    positions[line[0]][i] += offset[i]
    return positions


# ----- extended simpletransform.py

# pylint: disable=missing-docstring

def ident_mat():
    return [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]


def rootToNodeTransform(node, mat=None):
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        return invertTransform(
            simpletransform.composeParents(node, mat))
    else:
        return mat


def nodeToRootTransform(node, mat=None):
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        return simpletransform.composeParents(node, mat)
    else:
        return mat


def fromToTransform(node1, node2, mat=None):
    mat = ident_mat() if mat is None else mat
    return simpletransform.composeTransform(nodeToRootTransform(node1, mat),
                                            rootToNodeTransform(node2, mat))


def computePointInNode(pt, node, mat=None):
    point = list(pt)
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        simpletransform.applyTransformToPoint(
            rootToNodeTransform(node, mat), point)
    return point


def computePointInRoot(pt, node, mat=None):
    point = list(pt)
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        simpletransform.applyTransformToPoint(
            nodeToRootTransform(node, mat), point)
    return point

# pylint: enable=missing-docstring


# ----- SVG helper functions

def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


# ----- interpolate functions

def parameterize(x, a, b):
    """Parameterize x into interval defined by a and b."""
    return 0 if a == b else (x - a) / float(b - a)


def interpolate(t, a, b):
    """The reverse of parameterize()."""
    return a + float(b - a)*t


# ----- main class

class RemoveOverlaps(EffectCompat):
    """Effect-based class to remove bbox overlaps in stack order."""

    def __init__(self):
        """Init base class."""
        EffectCompat.__init__(self)

        # instance attributes
        self.positions = None

        # options
        self.OptionParser.add_option("--action",
                                     action="store", type="string",
                                     dest="action", default="append_selected",
                                     help="Selected action")
        self.OptionParser.add_option("--bbox_mode",
                                     action="store", type="string",
                                     dest="bbox_mode", default="visual",
                                     help="BBox mode (visual | geometric)")
        self.OptionParser.add_option("--direction",
                                     action="store", type="string",
                                     dest="direction", default="stack",
                                     help="Arrange along")
        self.OptionParser.add_option("--reverse_direction",
                                     action="store", type="inkbool",
                                     dest="reverse_direction", default=False,
                                     help="Reverse direction")
        self.OptionParser.add_option("--close_gaps",
                                     action="store", type="inkbool",
                                     dest="close_gaps", default=False,
                                     help="Close gaps")

    def geom_bboxes(self, id_list):
        """Compute geom bbox of nodes in id_list."""
        positions = {}
        for node_id in id_list:
            node = self.getElementById(node_id)
            mat = nodeToRootTransform(node.getparent())
            bbox_extents = simpletransform.computeBBox([node], mat)
            if bbox_extents is not None and len(bbox_extents) == 4:
                minx, maxx, miny, maxy = bbox_extents
                positions[node_id] = [minx, miny, maxx - minx, maxy - miny]
        return dict(positions)

    def visual_bboxes(self, id_list):
        """Compute visual bbox of nodes in id_list."""
        scale = self.unittouu('1px')
        offset = self.get_page_offset()
        return query_all(self.svg_file, id_list, scale, offset)

    def get_bboxes(self, id_list, mode=None):
        """Create list with bbox values for selected objects."""
        mode = self.options.bbox_mode if mode is None else mode
        func_name = '{}_bboxes'.format(mode)
        func = getattr(self, func_name, None)
        if func is not None:
            return func(id_list)

    def translate(self, node_id, values):
        """Translate object with node_id by offset."""
        # pylint: disable=too-many-locals
        node = self.getElementById(node_id)
        # parse delta
        [px, py], [dx, dy] = values
        # convert offset in root to actual node offset
        d = 'm {},{} {},{}'.format(px, py, dx, dy)
        csp = cubicsuperpath.parsePath(d)
        # compute matrix
        mat = rootToNodeTransform(node)
        node_trans = node.get('transform', None)
        if node_trans is not None:
            node_mat = simpletransform.parseTransform(node_trans)
            mat = simpletransform.composeTransform(node_mat, mat)
        # apply matrix to csp
        simpletransform.applyTransformToPath(mat, csp)
        # get new delta
        origin = csp[0][0][1]
        target = csp[0][-1][1]
        tx, ty = [target[i] - origin[i] for i in range(2)]
        # apply the move
        move = 'translate({},{})'.format(tx, ty)
        transform = node.get('transform', '')
        node.set('transform', ' '.join([move, transform]))

    def remove_overlaps(self, id_list, j=0):
        """Remove X or Y overlaps."""
        deltas = {}
        last = None
        # iterate over objects in id_list
        for node_id in id_list:
            if node_id in self.positions.keys():
                if last is None:
                    last = self.positions[node_id][j]
                current = self.positions[node_id][j]
                if self.options.close_gaps:
                    check_position = not isclose(current, last)
                else:
                    check_position = current < last
                if check_position:
                    delta = last - current
                    origin = self.positions[node_id][0:2]
                    offset = [delta * (1 - j), delta * j]
                    deltas[node_id] = [origin, offset]
                    self.positions[node_id][j] = last
                    last += self.positions[node_id][j+2]
                else:
                    last = current + self.positions[node_id][j+2]
        # re-arrange objects as needed
        for k, v in deltas.items():
            self.translate(k, v)

    def effect(self):
        """Main entry point to process current document."""
        id_list = []
        # get sorted list of ids
        if self.options.direction == 'stack':
            # remove overlaps along stack order
            root = self.document.getroot()
            id_list = z_sort(root, self.selected.keys())
        else:
            # TODO: remove overlaps along positions
            return
        if len(id_list):
            if self.options.reverse_direction:
                id_list.reverse()
            # get dictionary with object bboxes
            self.positions = self.get_bboxes(id_list)
            # run selected action
            if self.positions is not None and len(self.positions):
                j = 0 if self.options.action == 'overlap_x' else 1
                self.remove_overlaps(id_list, j)


if __name__ == '__main__':
    ME = RemoveOverlaps()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
