#!/usr/bin/env python
"""
toggle_bg - toggle document checkerboard background

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# inkscape library
try:
    import inkex_local as inkex
except ImportError:
    import inkex


__version__ = '0.0'


DEBUG = False


def get_namedview(doc):
    """Return <namedview/> document node in sodipodi namespace."""
    if doc is not None:
        path = '//sodipodi:namedview'
        result = doc.xpath(path, namespaces=inkex.NSS)
        if result:
            return result[0]
        else:
            return None


class ToggleBackground(inkex.Effect):
    """Effect-based class to reset IDs of selected objects."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # options
        self.OptionParser.add_option("--action",
                                     action="store", type="string",
                                     dest="action",
                                     default="toggle_bg",
                                     help="Selected action")

    def change_doc_prop(self, prop, new_val=None, toggle=False):
        """Change document property."""
        namedview = get_namedview(self.document.getroot())
        if namedview is not None:
            if toggle:
                switch = {'false': 'true', '0': '1',
                          'true': 'false', '1': '0'}
                namedview.set(prop, switch[namedview.get(prop, 'false')])
            else:
                old_val = namedview.get(prop)
                if DEBUG:
                    inkex.debug('Document property: {}'.format(prop))
                    inkex.debug('old: {}, new: {}'.format(old_val, new_val))
                namedview.set(prop, str(new_val))

    def effect(self):
        """Main entry point to process current document."""
        if self.options.action == 'toggle_bg':
            prop = inkex.addNS('pagecheckerboard', 'inkscape')
            self.change_doc_prop(prop, toggle=True)


if __name__ == '__main__':
    ME = ToggleBackground()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
