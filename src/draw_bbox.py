#!/usr/bin/env python
"""
draw_bbox - draw bounding box of selected objects

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors

# standard library
import csv
import time
from subprocess import Popen, PIPE

# inkscape library
try:
    import inkex_local as inkex
except ImportError:
    import inkex
import simpletransform

# local library
from transform_objects import EffectCompat, invertTransform

__version__ = '0.0'


# ----- general helper functions

def timed(f):
    """Minimalistic timer for functions."""
    # pylint: disable=invalid-name
    start = time.time()
    ret = f()
    elapsed = time.time() - start
    return ret, elapsed


# ----- query objects

def run_inkquery(command_format, stdin_str=None, verbose=False):
    """Run command"""
    if verbose:
        inkex.debug(command_format)
    out = err = None
    myproc = Popen(command_format, shell=False,
                   stdin=PIPE, stdout=PIPE, stderr=PIPE,
                   universal_newlines=True)
    out, err = myproc.communicate(stdin_str)
    if myproc.returncode == 0:
        return out
    elif err is not None:
        inkex.errormsg(err)


def query_all(svgfile, id_list, scale=1.0, offset=None):
    """Spawn external inkscape to query all objects in *svgfile*."""
    opts = ['inkscape']
    opts.append('--query-all')
    opts.append(svgfile)
    stdout_str = run_inkquery(opts, stdin_str=None, verbose=False)
    reader = csv.reader(stdout_str.splitlines())
    positions = {}
    for line in reader:
        if len(line) > 0 and line[0] in id_list:
            positions[line[0]] = [scale * float(v) for v in line[1:]]
            if offset is not None and len(offset) == 2:
                for i in range(2):
                    positions[line[0]][i] += offset[i]
    return positions


# ----- extended simpletransform.py

# pylint: disable=invalid-name
# pylint: disable=missing-docstring

def ident_mat():
    return [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]


def rootToNodeTransform(node, mat=None):
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        return invertTransform(
            simpletransform.composeParents(node, mat))
    else:
        return mat


def nodeToRootTransform(node, mat=None):
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        return simpletransform.composeParents(node, mat)
    else:
        return mat


def fromToTransform(node1, node2, mat=None):
    mat = ident_mat() if mat is None else mat
    return simpletransform.composeTransform(nodeToRootTransform(node1, mat),
                                            rootToNodeTransform(node2, mat))


def computePointInNode(pt, node, mat=None):
    point = list(pt)
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        simpletransform.applyTransformToPoint(
            rootToNodeTransform(node, mat), point)
    return point


def computePointInRoot(pt, node, mat=None):
    point = list(pt)
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        simpletransform.applyTransformToPoint(
            nodeToRootTransform(node, mat), point)
    return point

# pylint: enable=invalid-name
# pylint: enable=missing-docstring


# ----- SVG helper functions

def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


def is_rect(node):
    """Check whether node is SVG element type <rect/>."""
    return node.tag == inkex.addNS('rect', 'svg')


def bbox_extents_to_rect(extents):
    """Return bbox extents converted to rect data (x, y, w, h)."""
    minx, maxx, miny, maxy = extents
    return [minx, miny, maxx - minx, maxy - miny]


def bbox_rect_to_extents(bbox):
    """Return bbox rect converted to extents (minx, maxx, miny, maxy)."""
    minx, miny, width, height = bbox
    return [minx, minx + width, miny, miny + height]


def geom_bbox(node):
    """Return geometric bbox values of node in root coordinates."""
    node_mat = nodeToRootTransform(node.getparent())
    bbox_extents = simpletransform.computeBBox([node], node_mat)
    if bbox_extents is not None and len(bbox_extents) == 4:
        return bbox_extents_to_rect(bbox_extents)


# ----- draw helper functions

def bbox_default_style():
    """Return default style for visualized bbox shape or path."""
    return "fill:blue;fill-opacity:0.25;stroke:none"


def draw_bbox_path(bbox):
    """Draw path with bbox position and dimensions."""
    x, y, width, height = bbox
    path_d = ""
    path_d += "m {},{} ".format(x, y)
    path_d += "h {} ".format(width)
    path_d += "v {} ".format(height)
    path_d += "h {} ".format(-width)
    path_d += "z"
    bbox_path = inkex.etree.Element(inkex.addNS('path', 'svg'))
    bbox_path.set('d', path_d)
    bbox_path.set('style', bbox_default_style())
    return bbox_path


def draw_bbox_poly(bbox, closed=False):
    """Draw polyline or polygon with bbox position and dimensions."""
    x, y, width, height = bbox
    points = ""
    points += "{},{} ".format(x, y)
    points += "{},{} ".format(x + width, y)
    points += "{},{} ".format(x + width, y + height)
    points += "{},{} ".format(x, y + height)
    if closed:
        bbox_poly = inkex.etree.Element(inkex.addNS('polygon', 'svg'))
    else:
        bbox_poly = inkex.etree.Element(inkex.addNS('polyline', 'svg'))
    bbox_poly.set('points', points.strip())
    bbox_poly.set('style', bbox_default_style())
    return bbox_poly


def draw_bbox_rect(bbox):
    """Draw rect with bbox position and dimensions."""
    x, y, width, height = bbox
    bbox_rect = inkex.etree.Element(inkex.addNS('rect', 'svg'))
    bbox_rect.set('x', str(x))
    bbox_rect.set('y', str(y))
    bbox_rect.set('width', str(width))
    bbox_rect.set('height', str(height))
    bbox_rect.set('style', bbox_default_style())
    return bbox_rect


# ------ main class

class DrawBBox(EffectCompat):
    """Effect-based class to draw bbox of selected objects."""

    def __init__(self):
        """Init base class."""
        EffectCompat.__init__(self)

        # instance attributes
        self.bboxes = None

        # options
        self.OptionParser.add_option("--action",
                                     action="store", type="string",
                                     dest="action", default="draw_bbox",
                                     help="[hidden] Selected action")
        self.OptionParser.add_option("--bbox_mode",
                                     action="store", type="string",
                                     dest="bbox_mode", default="geom",
                                     help="BBox mode (geometric|visual)")
        self.OptionParser.add_option("--bbox_type",
                                     action="store", type="string",
                                     dest="bbox_type", default="rect",
                                     help="Object type to visualize bbox")
        self.OptionParser.add_option("--replace",
                                     action="store", type="inkbool",
                                     dest="replace", default=False,
                                     help="Replace original object")
        self.OptionParser.add_option("--copy_style",
                                     action="store", type="inkbool",
                                     dest="copy_style", default=False,
                                     help="Copy style from original object")

    def get_bboxes(self, id_list):
        """Return dict with visual or geom bboxes of selected objects."""
        bboxes = {}
        if self.options.bbox_mode == 'visual':
            # visual bbox in root coordinates
            scale = self.unittouu('1px')
            offset = self.get_page_offset()
            bboxes = query_all(self.svg_file, id_list, scale, offset)
        elif self.options.bbox_mode == 'geom':
            # geometric bbox in root coordinates
            for node_id in id_list:
                bbox = geom_bbox(self.selected[node_id])
                if bbox is not None:
                    bboxes[node_id] = bbox
        return bboxes if bboxes else None

    def create_bbox(self, node_id, node):
        """Visualize bounding box with rect or path."""
        bbox = None
        mat = ident_mat()
        # get bbox rect in SVG root coordinates
        root_bbox = self.bboxes.get(node_id)
        if root_bbox is not None and len(root_bbox) == 4:
            # create bbox object
            if self.options.bbox_type == 'rect':
                bbox = draw_bbox_rect(root_bbox)
            elif self.options.bbox_type == 'polyline':
                bbox = draw_bbox_poly(root_bbox, closed=False)
            elif self.options.bbox_type == 'polygon':
                bbox = draw_bbox_poly(root_bbox, closed=True)
            else:
                bbox = draw_bbox_path(root_bbox)
        if bbox is not None:
            # get transform from root to current node
            mat = rootToNodeTransform(node)
            transform = node.get('transform', None)
            if transform is not None:
                node_mat = simpletransform.parseTransform(transform)
                mat = simpletransform.composeTransform(node_mat, mat)
        return bbox, mat

    def draw_bbox(self, node_id):
        """Replace or underlay selected objects with bounding box."""
        node = self.selected[node_id]
        bbox, mat = self.create_bbox(node_id, node)
        if bbox is not None:
            parent = node.getparent()
            pos = parent.index(node)
            parent.insert(pos, bbox)
            # transform bbox object from root to node coordinates
            simpletransform.applyTransformToNode(mat, bbox)
            if self.options.copy_style:
                node_style = node.get('style')
                if node_style is not None:
                    bbox.set('style', node_style)
            if self.options.replace:
                bbox.set('id', node.get('id'))
                parent.remove(node)

    def effect(self):
        """Main entry point to process current document."""
        # get z-sorted list of selected objects
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        # cache dict with visual or geometrix bbox values
        self.bboxes = self.get_bboxes(id_list)
        # actions
        if self.bboxes is not None and len(self.bboxes) == len(id_list):
            if self.options.action == 'draw_bbox':
                for node_id in reversed(id_list):
                    self.draw_bbox(node_id)


if __name__ == '__main__':
    ME = DrawBBox()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
