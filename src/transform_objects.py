#!/usr/bin/env python
"""
transform_objects - transform object based on selection or object bbox

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors
# pylint: disable=too-many-lines
# pylint: disable=too-many-public-methods

# standard library
import copy
import csv
import math
from math import radians, cos, sin, tan, atan2, isnan
import random
import re
from subprocess import Popen, PIPE
import timeit

# inkscape library
try:
    import inkex_local as inkex
except ImportError:
    import inkex
import cubicsuperpath
try:
    from measure import cspcofm
except ImportError:
    pass
import simplestyle
from simpletransform import applyTransformToNode, applyTransformToPath
from simpletransform import composeParents, composeTransform
from simpletransform import parseTransform, computeBBox


__version__ = '0.0'


ANGULAR_UNITS = ['deg', 'grad', 'rad', 'turn']
LENGTH_UNITS = ['mm', 'cm', 'm', 'px', 'pt', 'pc', 'in', 'ft']
CUSTOM_UNITS = ['percent', 'display', 'uu']


# ----- general helper functions

def timed(f):
    """Minimalistic timer for functions."""
    # pylint: disable=invalid-name
    start = timeit.default_timer()
    ret = f()
    elapsed = timeit.default_timer() - start
    return ret, elapsed


def are_near_relative(a, b, eps):
    """Copied from upstream inkex.py for compat with inkex_local.py."""
    return (a-b <= a*eps) and (a-b >= -a*eps)


def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    """Test approximate equality.

    ref:
        PEP 485 -- A Function for testing approximate equality
        https://www.python.org/dev/peps/pep-0485/#proposed-implementation
    """
    # pylint: disable=invalid-name
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


def split_list_of_numbers(string):
    """Split white space- or comma-separated string into floats."""
    # pylint: disable=bad-builtin
    try:
        return [float(v) for v in filter(None, re.split(r'[\s,]', string))]
    except (ValueError, TypeError):
        return []


def parse_viewbox(node):
    """Return a list of floats representing viewBox attribute of node."""
    try:
        return split_list_of_numbers(node.get('viewBox'))
    except AttributeError:
        pass


# ----- query objects

def run_inkquery(command_format, stdin_str=None, verbose=False):
    """Run command"""
    if verbose:
        inkex.debug(command_format)
    out = err = None
    myproc = Popen(command_format, shell=False,
                   stdin=PIPE, stdout=PIPE, stderr=PIPE,
                   universal_newlines=True)
    out, err = myproc.communicate(stdin_str)
    if myproc.returncode == 0:
        return out
    elif err is not None:
        inkex.errormsg(err)


def query_all(svgfile, id_list=None, scale=1.0, offset=None):
    """Spawn external inkscape to query all objects in *svgfile*."""
    opts = ['inkscape']
    opts.append('--query-all')
    opts.append(svgfile)
    stdout_str = run_inkquery(opts, stdin_str=None, verbose=False)
    reader = csv.reader(stdout_str.splitlines())
    positions = {}
    for line in reader:
        if len(line) and (id_list is None or line[0] in id_list):
            positions[line[0]] = [scale * float(v) for v in line[1:]]
            if offset is not None and len(offset) == 2:
                for i in range(2):
                    positions[line[0]][i] += offset[i]
    return positions


# ----- extended simpletransform.py

# pylint: disable=invalid-name
# pylint: disable=missing-docstring

def ident_mat():
    return [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]


def invertTransform(mat):
    # NOTE: copied here from simpletransform.py to support tests with <= 0.91
    det = mat[0][0]*mat[1][1] - mat[0][1]*mat[1][0]
    if det != 0:  # det is 0 only in case of 0 scaling
        # invert the rotation/scaling part
        a11 = mat[1][1]/det
        a12 = -mat[0][1]/det
        a21 = -mat[1][0]/det
        a22 = mat[0][0]/det
        # invert the translational part
        a13 = -(a11*mat[0][2] + a12*mat[1][2])
        a23 = -(a21*mat[0][2] + a22*mat[1][2])
        return [[a11, a12, a13], [a21, a22, a23]]
    else:
        return[[0, 0, -mat[0][2]], [0, 0, -mat[1][2]]]


def get_transformed_point(mat, point):
    x = mat[0][0]*point[0] + mat[0][1]*point[1] + mat[0][2]
    y = mat[1][0]*point[0] + mat[1][1]*point[1] + mat[1][2]
    return [x, y]


def get_mat_from_root_to_node(node, mat=None):
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        return invertTransform(composeParents(node, mat))
    else:
        return mat


def get_mat_from_node_to_root(node, mat=None):
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        return composeParents(node, mat)
    else:
        return mat


def get_mat_from_to(node1, node2, mat=None):
    mat = ident_mat() if mat is None else mat
    return composeTransform(get_mat_from_node_to_root(node1, mat),
                            get_mat_from_root_to_node(node2, mat))


def get_point_in_node(pt, node, mat=None):
    point = list(pt)
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        point = get_transformed_point(get_mat_from_root_to_node(node, mat),
                                      point)
    return point


def get_point_in_root(pt, node, mat=None):
    point = list(pt)
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        point = get_transformed_point(get_mat_from_node_to_root(node, mat),
                                      point)
    return point


def get_mat_of(node):
    if 'transform' in node.attrib:
        return parseTransform(node.get('transform'))
    else:
        return ident_mat()


def get_mat_composed_in_root(node, center, mat_transform):
    """Compose mat_transform for node applied in SVG root."""
    node_mat = get_mat_of(node)
    root_mat = get_mat_from_root_to_node(node)
    if center is None:
        center_mat = ident_mat()
    else:
        center_mat = [[1.0, 0.0, center[0]],
                      [0.0, 1.0, center[1]]]
    return composeTransform(
        node_mat,
        composeTransform(
            root_mat,
            composeTransform(
                center_mat,
                composeTransform(
                    mat_transform,
                    composeTransform(
                        invertTransform(center_mat),
                        composeTransform(
                            invertTransform(root_mat),
                            invertTransform(node_mat)))))))


def get_mat_composed_in_node(node, center, mat_transform):
    """Compose mat_transform for SVG root applied in node coords."""
    node_mat = get_mat_of(node)
    root_mat = get_mat_from_root_to_node(node)
    if center is None:
        center_mat = ident_mat()
    else:
        center_mat = [[1.0, 0.0, center[0]],
                      [0.0, 1.0, center[1]]]
    return composeTransform(
        invertTransform(center_mat),
        composeTransform(
            invertTransform(root_mat),
            composeTransform(
                invertTransform(node_mat),
                composeTransform(
                    mat_transform,
                    composeTransform(
                        node_mat,
                        composeTransform(
                            root_mat,
                            center_mat))))))


# pylint: enable=invalid-name
# pylint: enable=missing-docstring


# ----- SVG helper functions

def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


def is_rect(node):
    """Check whether node is SVG element type <rect/>."""
    return node.tag == inkex.addNS('rect', 'svg')


def bbox_extents_to_rect(extents):
    """Return bbox extents converted to rect data (x, y, w, h)."""
    minx, maxx, miny, maxy = extents
    return [minx, miny, maxx - minx, maxy - miny]


def bbox_rect_to_extents(bbox):
    """Return bbox rect converted to extents (minx, maxx, miny, maxy)."""
    minx, miny, width, height = bbox
    return [minx, minx + width, miny, miny + height]


def get_geom_bbox(node):
    """Return geometric bbox values of node in root coordinates."""
    node_mat = get_mat_from_node_to_root(node.getparent())
    bbox_extents = computeBBox([node], node_mat)
    if bbox_extents is not None and len(bbox_extents) == 4:
        return bbox_extents_to_rect(bbox_extents)


def get_rect_anchor(rect, ref=None, coords="svg"):
    """Return anchor point of rect."""
    x, y, width, height = rect
    ref = ('midx', 'midy') if ref is None else ref
    anchors_x = {'minx': x,
                 'midx': x + width/2.0,
                 'maxx': x + width}
    if coords == 'desktop':
        anchors_y = {'miny': y + height,
                     'midy': y + height/2.0,
                     'maxy': y}
    else:
        anchors_y = {'miny': y,
                     'midy': y + height/2.0,
                     'maxy': y + height}
    return [anchors_x[ref[0]], anchors_y[ref[1]]]


def get_rect_csp(rect, center=False):
    """Return csp path for rect."""
    x, y, width, height = rect
    path_d = ''
    # 3 segments are connecting the corners of the rect
    path_d += 'm {},{} '.format(x, y)
    path_d += 'h {} '.format(width)
    path_d += 'v {} '.format(height)
    path_d += 'h {} '.format(-width)
    # last segment is a half-diagonal to the center of the rect
    if center:
        path_d += 'L {},{}'.format(x + width/2.0, y + height/2.0)
    return cubicsuperpath.parsePath(path_d)


def check_props(node, is_copy=False):
    """Purge unwanted attributes from copied node."""
    # create list with attributes to remove
    del_props = []

    # attribute 'inkscape:connector-curvature':
    # might contribute to crash on reload with rev >= 14947
    del_props.append(inkex.addNS('connector-curvature', 'inkscape'))

    # 'inkscape:transform-center-x', 'inkscape:transform-center-y':
    # these contribute to the crash, but also on the original objects
    # they should not be purged, since usually are set intentionally.
    # del_props.append(inkex.addNS('transform-center-x', 'inkscape'))
    # del_props.append(inkex.addNS('transform-center-y', 'inkscape'))

    # attribute 'svg:id':
    # on request, delete the copied id and let inkscape assign a new id on
    # reload
    if is_copy:
        del_props.append('id')

    # clean up
    for prop in del_props:
        # q_name = inkex.etree.QName(prop)
        # inkex.debug('removing {} ...'.format(q_name.localname))
        node.attrib.pop(prop, None)

    # TODO: there could be checks that possibly fail and return False?
    return True


def make_copy(node, mode="duplicate"):
    """Create a copy of node.

    Return the original or the new node based on *mode*.
    """
    parent = node.getparent()
    index = parent.index(node)
    new_node = copy.deepcopy(node)
    if check_props(new_node, is_copy=True):
        if mode == 'drop':
            # insert copy before, return original
            parent.insert(index, new_node)
            return node
        elif mode == 'stamp':
            # insert copy after, return copy
            parent.insert(index+1, new_node)
            return new_node
        else:
            # append copy to parent, return copy
            parent.append(new_node)
            return new_node
    else:
        return node


def make_clone(node, mode="stamp"):
    """Create a clone of node.

    Return the original or the new node based on *mode*.
    """
    parent = node.getparent()
    index = parent.index(node)
    href = '#{}'.format(node.get('id'))
    new_node = inkex.etree.Element(inkex.addNS('use', 'svg'))
    new_node.set(inkex.addNS('href', 'xlink'), href)
    if mode == 'drop':
        # insert clone before, return original
        parent.insert(index, new_node)
        return node
    elif mode == 'stamp':
        # insert clone after, return clone
        parent.insert(index+1, new_node)
        return new_node
    else:
        # append clone to parent, return clone
        parent.append(new_node)
        return new_node


# ----- Inkscape transformation center

def has_inkscape_transform_center(node):
    """Check if node has Inkscape transformation center defined."""
    return (inkex.addNS('transform-center-x', 'inkscape') in node.attrib or
            inkex.addNS('transform-center-y', 'inkscape') in node.attrib)


def get_inkscape_transform_center(node, bbox_center):
    """Return coordinates of Inkscape's transform center."""
    dx = node.get(inkex.addNS('transform-center-x', 'inkscape'), 0)
    dy = node.get(inkex.addNS('transform-center-y', 'inkscape'), 0)
    return [bbox_center[0] + float(dx),
            bbox_center[1] - float(dy)]


def set_inkscape_transform_center(node, bbox_center, point):
    """Set coordinates of Inkscape's transform center.

    Parameter *point* is defined in SVG coordinates with user units."""
    dx = point[0] - bbox_center[0]
    dy = -1 * (point[1] - bbox_center[1])
    node.set(inkex.addNS('transform-center-x', 'inkscape'), str(dx))
    node.set(inkex.addNS('transform-center-y', 'inkscape'), str(dy))


def add_cross(node, center, radius=3.0, scale=1.0):
    """Add path data for cross to node."""
    x, y = center
    radius *= scale
    path_d = ''
    # horizontal diameter
    path_d += 'm {},{} '.format(x-radius, y)
    path_d += 'h {}'.format(radius)
    path_d += 'h {} '.format(radius)
    # vertical diameter
    path_d += 'm {},{} '.format(-radius, -radius)
    path_d += 'v {} '.format(radius)
    path_d += 'v {}'.format(radius)
    node.set('d', path_d)
    sdict = {
        'fill': 'none',
        'stroke': 'black',
        'stroke-width': str(0.5 * scale),
    }
    node.set('style', simplestyle.formatStyle(sdict))


def add_x(node, center, radius=3.0, scale=1.0):
    """Add path data for cross to node."""
    x, y = center
    radius *= scale
    path_d = ''
    # first diagonal
    path_d += 'm {},{} '.format(x-radius, y-radius)
    path_d += 'l {},{}'.format(radius, radius)
    path_d += 'l {},{} '.format(radius, radius)
    # second diagonal
    path_d += 'm 0,{} '.format(-2*radius)
    path_d += 'l {},{} '.format(-radius, radius)
    path_d += 'l {},{}'.format(-radius, radius)
    node.set('d', path_d)
    sdict = {
        'fill': 'none',
        'stroke': 'black',
        'stroke-width': str(0.5 * scale),
    }
    node.set('style', simplestyle.formatStyle(sdict))


# ----- stretch objects

def stretch_factor(check, length, new_length, anchor):
    """Return stretch factor and anchor based on check."""
    ratio = 1.0
    if check:
        try:
            ratio = new_length / length
        except ZeroDivisionError:
            anchor = None
    else:
        anchor = None
    return ratio, anchor


# ----- mirror line

def is_path(node):
    """Check whether node is of type SVG path."""
    return node.tag == inkex.addNS('path', 'svg')


def get_reflect_points(node):
    """Return start and end node of *node* as pt1, pt2 in root."""
    csp = cubicsuperpath.parsePath(node.get('d'))
    if len(csp):
        mat = composeTransform(get_mat_from_node_to_root(node.getparent()),
                               get_mat_of(node))
        applyTransformToPath(mat, csp)
        try:
            pt1 = csp[0][0][1][:]
            pt2 = csp[-1][-1][1][:]
        except KeyError:
            pt1 = pt2 = []
        if len(pt1) == 2 and len(pt2) == 2:
            return (pt1, pt2)


def get_reflect_slope(pt1, pt2):
    """Return slope of line pt1, pt2."""
    dx = pt2[0] - pt1[0]
    dy = pt2[1] - pt1[1]
    if not dx:
        slope = float('NaN')
    elif not dy:
        slope = 0.0
    else:
        slope = dy / dx
    return slope


def get_reflect_angle_x(pt1, pt2):
    """Return angle to x-axis of line pt1, pt2."""
    dx = pt2[0] - pt1[0]
    dy = pt2[1] - pt1[1]
    return atan2(dy, dx)


def get_reflect_angle_y(pt1, pt2):
    """Return angle to y-axis of line pt1, pt2."""
    dx = pt2[0] - pt1[0]
    dy = pt2[1] - pt1[1]
    return atan2(dx, dy)


def get_reflect_intercept_x(pt1, pt2):
    """Return x coordinate of line pt1, pt2 intercept with x-axis."""
    slope = get_reflect_slope(pt1, pt2)
    if slope == 0:
        intercept = float('NaN')
    elif isnan(slope):
        intercept = pt1[0]
    else:
        intercept = pt1[0] - (pt1[1] / slope)
    return intercept


def get_reflect_intercept_y(pt1, pt2):
    """Return y coordinate of line pt1, pt2 intercept with y-axis."""
    slope = get_reflect_slope(pt1, pt2)
    if slope == 0:
        intercept = pt1[1]
    elif isnan(slope):
        intercept = float('NaN')
    else:
        intercept = pt1[1] - (pt1[0] * slope)
    return intercept


def get_mat_reflect(phi):
    """Return reflection matrix for angle phi."""
    return [[cos(2*phi), sin(2*phi), 0.0],
            [sin(2*phi), -cos(2*phi), 0.0]]


# ----- align or distribute text

def is_flowed_text(node):
    """Check whether node is <flowRoot> type."""
    return node.tag == inkex.addNS('flowRoot', 'svg')


def is_regular_text(node):
    """Check whether node is <text> or <flowRoot> type."""
    return node.tag == inkex.addNS('text', 'svg')


def is_text(node):
    """Check whether node is <text> or <flowRoot> type."""
    return is_regular_text(node) or is_flowed_text(node)


def get_text_pos(node, root=False):
    """Return coordinates of <text> element."""
    if is_text(node):
        pos = [float(node.get('x', 0.0)),
               float(node.get('y', 0.0))]
        if root:
            pos = get_point_in_root(pos, node)
        return pos


def set_text_pos(node, point):
    """Set coordinates of text node."""
    if is_text(node):
        node.set('x', str(point[0]))
        node.set('y', str(point[1]))


# ----- workarounds for upstream issues

def tab_name(name):
    """Workaround for Inkscape's notebook page parameter value."""
    return name.strip('"')


# ------ geom classes (from image_lib)

class Point(object):
    """Point base class for storing points in cartesian coordinates."""

    def __init__(self, x=0, y=0):
        """Init Point based on cartesian coordinates x, y."""
        self._x = float(x)
        self._y = float(y)

    # Special methods

    def __eq__(self, other):
        assert isinstance(other, Point)
        return other.x == self.x and other.y == self.y

    def __ne__(self, other):
        assert isinstance(other, Point)
        return other.x != self.x or other.y != self.y

    def __lt__(self, other):
        assert isinstance(other, Point)
        return self.r < other.r

    def __le__(self, other):
        assert isinstance(other, Point)
        return self.r <= other.r

    def __gt__(self, other):
        assert isinstance(other, Point)
        return self.r > other.r

    def __ge__(self, other):
        assert isinstance(other, Point)
        return self.r >= other.r

    def __pos__(self):
        return Point(self.x, self.y)

    def __neg__(self):
        return Point(-self.x, -self.y)

    def __abs__(self):
        return Point(abs(self.x), abs(self.y))

    def __invert__(self):
        return Point(self.y, self.x)

    def __add__(self, other):
        assert isinstance(other, Point)
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        assert isinstance(other, Point)
        return Point(self.x - other.x, self.y - other.y)

    def __mul__(self, factor):
        return Point(self.x * factor, self.y * factor)

    def __div__(self, quotient):
        return Point(self.x / quotient, self.y / quotient)

    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y

    def __isub__(self, other):
        self.x -= other.x
        self.y -= other.y

    def __imul__(self, other):
        new = self * other
        self.x = new.x
        self.y = new.y

    def __idiv__(self, other):
        new = self / other
        self.x = new.x
        self.y = new.y

    def __iter__(self):
        return self.__list__().__iter__()

    def __list__(self):
        return [self.x, self.y]

    def __dict__(self):
        return {"x": self.x, "y": self.y}

    def __copy__(self):
        return Point(self.x, self.y)

    def __deepcopy__(self, memo):
        return Point(self.x, self.y)

    # Properties with setters

    @property
    def x(self):
        """Get or set the cartesian coordinate x."""
        return self._x

    @x.setter
    def x(self, x):
        self._x = x

    @property
    def y(self):
        """Get or set the cartesian coordinate y."""
        return self._y

    @y.setter
    def y(self, y):
        self._y = y

    @property
    def r(self):
        """Get or set the polar distance r (radius).

        The polar coordinates r, t are converted from/to
        properties x, y.
        """
        return math.sqrt(self.x ** 2 + self.y ** 2)

    @r.setter
    def r(self, radius):
        theta = self.t
        self.x = math.cos(theta) * radius
        self.y = math.sin(theta) * radius

    @property
    def t(self):
        """Get or set the polar angle t (theta) in radians.

        The polar coordinates r, t are converted from/to
        properties x, y.
        """
        return math.atan2(self.y, self.x)

    @t.setter
    def t(self, theta):
        radius = self.r
        self.x = math.cos(theta) * radius
        self.y = math.sin(theta) * radius

    @property
    def t_deg(self):
        """Get or set the polar angle t (theta) in degrees.

        The polar coordinates r, t are converted from/to
        properties x, y.
        """
        return math.degrees(self.t)

    @t_deg.setter
    def t_deg(self, theta):
        self.t = math.radians(theta)


class Polar(Point):
    """Point sub-class for points in polar coordinates (radius, theta)."""

    def __init__(self, radius=0.0, theta=0.0):
        """Init Point based on polar coordinates r, t."""
        Point.__init__(self)
        self._x = math.cos(theta) * radius
        self._y = math.sin(theta) * radius


# ------ main classes

class EffectCompat(inkex.Effect):
    """Wrapper for inkex.Effect() with compatibility methods."""
    # pylint: disable=line-too-long
    # pylint: disable=invalid-name
    # pylint: disable=too-many-locals
    # pylint: disable=too-many-public-methods
    # pylint: disable=no-member

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # Protected copy of the unit conversion table as instance attribute
        if hasattr(inkex, 'uuconv'):
            # Inkscape 0.48 and earlier
            self.__uuconv = dict(inkex.uuconv)
        elif hasattr(inkex.Effect, '_Effect__uuconv'):
            # Inkscape 0.91 and later
            self.__uuconv = dict(inkex.Effect._Effect__uuconv)
        elif hasattr(inkex, 'UUCONV'):
            # inkex replacement 'inkex_local.py'
            self.__uuconv = dict(inkex.UUCONV)
        # Key name of the unit conversion table record for the userunit scale
        # factor of the current document. This key needs to be unique and to
        # avoid conflicts with existing unit identifiers or whatever other
        # strings might be looked up in the unit conversion table. It is not
        # exposed directly in derived classes (use class methods below for
        # access).
        self.__uu_key = '{:08x}'.format(random.randrange(16**8)).upper()

    # ----- helper methods for document scale and unit conversions

    def known_units(self):
        """Return list of known units from unit conversion table."""
        return [k for k in self.__uuconv.keys() if k != self.__uu_key]

    def parse_length(self, string, val=100.0, unit='px'):
        """Parse length string into list with float and unit."""

        param = re.compile(
            r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)')
        units = re.compile('(%s)$' % '|'.join(self.known_units()))

        p = param.match(string)
        u = units.search(string)

        if p:
            val = float(p.string[p.start():p.end()])
        if u:
            unit = u.string[u.start():u.end()]

        return (val, unit)

    def compute_length(self, string):
        """Convert length string to computed value in CSS px."""
        val, unit = self.parse_length(string)
        try:
            return val * self.__uuconv[unit]
        except KeyError:
            return val

    def check_doc_scale_cache(self):
        """Check unit conversion table for userunit scale factor."""
        try:
            return bool(self.__uuconv[self.__uu_key])
        except KeyError:
            return False

    def get_doc_scale_cache(self):
        """Return userunit scale factor record in unit conversion table."""
        try:
            return self.__uuconv[self.__uu_key]
        except KeyError:
            return None

    def update_doc_scale_cache(self, value):
        """Update userunit scale factor record in unit conversion table."""
        self.__uuconv[self.__uu_key] = value

    def clear_doc_scale_cache(self):
        """Delete userunit scale factor record in unit conversion table."""
        return self.__uuconv.pop(self.__uu_key, None)

    # ----- methods to retrieve document properties

    def get_document_viewbox(self):
        """Return a list with viewBox values."""
        return parse_viewbox(self.document.getroot())

    def get_document_width(self):
        """Return a string corresponding to the width of the document."""
        width = self.document.getroot().get('width')
        if width:
            return width
        else:
            viewbox = self.get_document_viewbox()
            return str(viewbox[2]) if viewbox and len(viewbox) == 4 else '0'

    def get_document_height(self):
        """Return a string corresponding to the height of the document."""
        height = self.document.getroot().get('height')
        if height:
            return height
        else:
            viewbox = self.get_document_viewbox()
            return str(viewbox[3]) if viewbox and len(viewbox) == 4 else '0'

    def get_document_scale(self):
        """Return current document scale (absolute CSS px to userunits)."""
        # matches internal function SPDocument::getDocumentScale():
        # https://gitlab.com/inkscape/inkscape/blob/master/src/document.cpp#L631

        # Use the cached record if already stored in the instance's unit
        # conversion table, else compute the userunit scale factor of the
        # current document.
        if not self.check_doc_scale_cache():

            match = False
            uu_scale = 1.0
            viewbox = self.get_document_viewbox()

            if viewbox and len(viewbox) == 4:
                # compute userunit scale factor
                if viewbox[2] > 0:
                    # try viewBox width for x scale factor
                    width = self.get_document_width()
                    uu_scale = self.compute_length(width) / viewbox[2]
                if not uu_scale and viewbox[3] > 0:
                    # if x scale factor failed, try
                    # viewBox height for y scale factor
                    height = self.get_document_height()
                    uu_scale = self.compute_length(height) / viewbox[3]
                # check scale factor
                if not uu_scale:
                    uu_scale = 1.0
                # try to match uu_scale with a known scale factor.
                eps = 0.01  # allow 1% error in factor
                for value in self.__uuconv.values():
                    if are_near_relative(value, uu_scale, eps):
                        match = True
                        self.update_doc_scale_cache(value)
                        break

            if not match:
                # missing or invalid viewBox, or custom userunit scale factor
                self.update_doc_scale_cache(uu_scale)

        return self.get_doc_scale_cache()

    # ----- methods to convert lengths to different units

    def unittounit(self, val, from_unit, to_unit):
        """Return value converted from one unit to another unit."""

        if hasattr(self, 'inx_unit_to_unit'):
            return self.inx_unit_to_unit(val, from_unit, to_unit)

        try:
            return val * (self.__uuconv[from_unit] / self.__uuconv[to_unit])
        except KeyError:
            return val

    # ----- overlay to support arbitrary document scale

    def unittouu(self, string):
        """Return userunits given a string representation of a length."""

        if hasattr(self, 'inx_unit_to_uu'):
            return self.inx_unit_to_uu(string)

        val, unit = self.parse_length(string, val=0.0, unit='px')
        try:
            val_computed = val * self.__uuconv[unit]
            return val_computed / self.get_document_scale()
        except KeyError:
            return val

    def uutounit(self, val, unit):
        """Return value converted from userunits to other unit."""

        if hasattr(self, 'inx_uu_to_unit'):
            return self.inx_uu_to_unit(val, unit)

        try:
            val_computed = val * self.get_document_scale()
            return val_computed / self.__uuconv[unit]
        except KeyError:
            return val

    # ----- workaround to fix Effect() performance with large selections

    def collect_ids(self, doc=None):
        """Iterate all elements, build id dicts (doc_ids, selected)."""
        doc = self.document if doc is None else doc
        id_list = list(self.options.ids)
        for node in doc.getroot().iter(tag=inkex.etree.Element):
            if 'id' in node.attrib:
                node_id = node.get('id')
                self.doc_ids[node_id] = 1
                if node_id in id_list:
                    self.selected[node_id] = node
                    id_list.remove(node_id)

    def getselected(self):
        """Overload Effect() method."""
        self.collect_ids()

    def getdocids(self):
        """Overload Effect() method."""
        pass

    # ----- extended methods to retrieve specific geometry information

    def get_page_extents(self):
        """Return extents of viewport in user units."""
        if hasattr(self, 'inx_get_viewport_prop'):
            minx, miny, width, height = self.inx_get_viewport_prop('rect')
            return (minx, minx+width, miny, miny+height)
        else:
            # TODO: take viewport offset into account
            width, height = (self.unittouu(self.get_document_width()),
                             self.unittouu(self.get_document_height()))
            return (0.0, width, 0.0, height)

    def get_page_rect(self):
        """Return viewport as rect in user units."""
        if hasattr(self, 'inx_get_viewport_prop'):
            return self.inx_get_viewport_prop('rect')
        else:
            # TODO: take viewport offset into account
            width, height = (self.unittouu(self.get_document_width()),
                             self.unittouu(self.get_document_height()))
            return (0.0, 0.0, width, height)

    def get_page_offset(self):
        """Return offset of viewport in user units."""
        if 0:
            return self.get_page_rect()[0:2]
        if hasattr(self, 'inx_get_viewport_prop'):
            offset = self.inx_get_viewport_prop('rect')[:2]
        else:
            # TODO: take viewport offset into account
            offset = None
        return offset

    def get_page_size(self):
        """Return size of viewport in user units."""
        if hasattr(self, 'inx_get_viewport_prop'):
            page_rect = self.inx_get_viewport_prop('rect')
            page = (page_rect[2], page_rect[3])
        else:
            page = (self.unittouu(self.get_document_width()),
                    self.unittouu(self.get_document_height()))
        return page

    def get_page_width(self):
        """Return width of viewport in user units."""
        if hasattr(self, 'inx_get_viewport_prop'):
            pagewidth = self.inx_get_viewport_prop('width')
        else:
            pagewidth = self.unittouu(self.get_document_width())
        return pagewidth

    def get_page_height(self):
        """Return height of viewport in user units."""
        if hasattr(self, 'inx_get_viewport_prop'):
            pageheight = self.inx_get_viewport_prop('height')
        else:
            pageheight = self.unittouu(self.get_document_height())
        return pageheight

    def get_page_center(self):
        """Return center of page area in user units."""
        if hasattr(self, 'inx_get_viewport_prop'):
            pagecenter = self.inx_get_viewport_prop('center')
        else:
            # TODO: take viewport offset into account
            pagewidth = self.unittouu(self.get_document_height())
            pageheight = self.unittouu(self.get_document_height())
            pagecenter = (pagewidth / 2.0, pageheight / 2.0)
        return pagecenter

    def convert_to_display_units(self, val):
        """Convert value from SVG user units to inkscape display unit."""
        if hasattr(self, 'inx_get_display_prop'):
            display_unit = self.inx_get_display_prop('unit')
        else:
            display_unit = self.getNamedView().get(
                inkex.addNS('document-units', 'inkscape'), 'px')
        return self.uutounit(val, display_unit)

    def convert_from_display_units(self, val):
        """Convert value from inkscape display unit to SVG user units."""
        if hasattr(self, 'inx_get_display_prop'):
            display_unit = self.inx_get_display_prop('unit')
        else:
            display_unit = self.getNamedView().get(
                inkex.addNS('document-units', 'inkscape'), 'px')
        return val * self.unittouu('1{}'.format(display_unit))


class TransformBasic(EffectCompat):
    """Effect-based class with basic methods to transform SVG objects."""
    # pylint: disable=too-many-statements

    def __init__(self):
        """Init base class."""
        EffectCompat.__init__(self)

        # instance attributes
        self.bboxes = None
        self.bboxes_extents = None
        self.bboxes_center = None
        self.drawing_area = None

        # move cartesian
        self.OptionParser.add_option("--tx",
                                     action="store", type="float",
                                     dest="tx", default=0.0,
                                     help="Horizontal move")
        self.OptionParser.add_option("--tx_unit",
                                     action="store", type="string",
                                     dest="tx_unit", default="display",
                                     help="Unit for horizontal move")
        self.OptionParser.add_option("--tx_relative",
                                     action="store", type="inkbool",
                                     dest="tx_relative", default=True,
                                     help="Relative horizontal move")
        self.OptionParser.add_option("--ty",
                                     action="store", type="float",
                                     dest="ty", default=0.0,
                                     help="Vertical move")
        self.OptionParser.add_option("--ty_unit",
                                     action="store", type="string",
                                     dest="ty_unit", default="display",
                                     help="Unit for vertical move")
        self.OptionParser.add_option("--ty_relative",
                                     action="store", type="inkbool",
                                     dest="ty_relative", default=True,
                                     help="Relative vertical move")
        self.OptionParser.add_option("--translate_relative",
                                     action="store", type="inkbool",
                                     dest="translate_relative", default=True,
                                     help="Relative move")
        # move polar
        self.OptionParser.add_option("--polar_distance",
                                     action="store", type="float",
                                     dest="polar_distance", default=100,
                                     help="polar_distance")
        self.OptionParser.add_option("--polar_distance_unit",
                                     action="store", type="string",
                                     dest="polar_distance_unit",
                                     default="percent",
                                     help="Unit for polar_distance")
        self.OptionParser.add_option("--polar_distance_relative",
                                     action="store", type="inkbool",
                                     dest="polar_distance_relative",
                                     default=True,
                                     help="Polar distance is relative")
        self.OptionParser.add_option("--polar_angle",
                                     action="store", type="float",
                                     dest="polar_angle", default=0,
                                     help="polar_angle")
        self.OptionParser.add_option("--polar_angle_unit",
                                     action="store", type="string",
                                     dest="polar_angle_unit",
                                     default="deg",
                                     help="Unit for polar_angle")
        self.OptionParser.add_option("--polar_angle_direction",
                                     action="store", type="string",
                                     dest="polar_angle_direction",
                                     default="ccw",
                                     help="Polar angle direction")
        self.OptionParser.add_option("--polar_angle_relative",
                                     action="store", type="inkbool",
                                     dest="polar_angle_relative",
                                     default=True,
                                     help="Polar angle is relative")
        # scale options
        self.OptionParser.add_option("--sx",
                                     action="store", type="float",
                                     dest="sx", default=100,
                                     help="Width")
        self.OptionParser.add_option("--sx_unit",
                                     action="store", type="string",
                                     dest="sx_unit", default="percent",
                                     help="Unit for width")
        self.OptionParser.add_option("--sy",
                                     action="store", type="float",
                                     dest="sy", default=100,
                                     help="Height")
        self.OptionParser.add_option("--sy_unit",
                                     action="store", type="string",
                                     dest="sy_unit", default="percent",
                                     help="Unit for height")
        self.OptionParser.add_option("--scale_proportionally",
                                     action="store", type="inkbool",
                                     dest="scale_proportionally",
                                     default=False,
                                     help="Scale proportionally")
        self.OptionParser.add_option("--scale_position_only",
                                     action="store", type="inkbool",
                                     dest="scale_position_only",
                                     default=False,
                                     help="Scale only position of objects")
        self.OptionParser.add_option("--scale_position_refpoint",
                                     action="store", type="string",
                                     dest="scale_position_refpoint",
                                     default="bbox",
                                     help="Reference point for polar distance")
        # rotate options
        self.OptionParser.add_option("--rotate_angle",
                                     action="store", type="float",
                                     dest="rotate_angle", default=0.0,
                                     help="Rotation angle")
        self.OptionParser.add_option("--rotate_angle_unit",
                                     action="store", type="string",
                                     dest="rotate_angle_unit", default="deg",
                                     help="Rotation angle unit")
        self.OptionParser.add_option("--rotate_angle_direction",
                                     action="store", type="string",
                                     dest="rotate_angle_direction",
                                     default="ccw",
                                     help="Rotation angle direction")
        # skew options
        self.OptionParser.add_option("--skew_x",
                                     action="store", type="float",
                                     dest="skew_x", default=0.0,
                                     help="Skew X value")
        self.OptionParser.add_option("--skew_x_unit",
                                     action="store", type="string",
                                     dest="skew_x_unit", default="deg",
                                     help="Skew X unit")
        self.OptionParser.add_option("--skew_y",
                                     action="store", type="float",
                                     dest="skew_y", default=0.0,
                                     help="Skew Y value")
        self.OptionParser.add_option("--skew_y_unit",
                                     action="store", type="string",
                                     dest="skew_y_unit", default="deg",
                                     help="Skew Y unit")
        # matrix options
        self.OptionParser.add_option("--matrix_a",
                                     action="store", type="float",
                                     dest="matrix_a", default=1.0,
                                     help="Matrix a")
        self.OptionParser.add_option("--matrix_b",
                                     action="store", type="float",
                                     dest="matrix_b", default=0.0,
                                     help="Matrix b")
        self.OptionParser.add_option("--matrix_c",
                                     action="store", type="float",
                                     dest="matrix_c", default=0.0,
                                     help="Matrix c")
        self.OptionParser.add_option("--matrix_d",
                                     action="store", type="float",
                                     dest="matrix_d", default=1.0,
                                     help="Matrix d")
        self.OptionParser.add_option("--matrix_e",
                                     action="store", type="float",
                                     dest="matrix_e", default=0.0,
                                     help="Matrix e")
        self.OptionParser.add_option("--matrix_f",
                                     action="store", type="float",
                                     dest="matrix_f", default=0.0,
                                     help="Matrix f")
        # stretch options
        self.OptionParser.add_option("--stretch_target",
                                     action="store", type="string",
                                     dest="stretch_target", default="xmin",
                                     help="stretch_target")
        # common options
        self.OptionParser.add_option("--bbox_mode",
                                     action="store", type="string",
                                     dest="bbox_mode", default="geom",
                                     help="Bounding box mode")
        self.OptionParser.add_option("--coord_system",
                                     action="store", type="string",
                                     dest="coord_system", default="desktop",
                                     help="Coordinate system for input")
        # transformation center: object refpoint
        self.OptionParser.add_option("--transform_object_refpoint",
                                     action="store", type="string",
                                     dest="transform_object_refpoint",
                                     default="bbox",
                                     help="Object reference point")
        self.OptionParser.add_option("--bbox_anchor_x",
                                     action="store", type="string",
                                     dest="bbox_anchor_x", default="midx",
                                     help="X anchor relative to bbox")
        self.OptionParser.add_option("--bbox_anchor_y",
                                     action="store", type="string",
                                     dest="bbox_anchor_y", default="midy",
                                     help="Y anchor relative to bbox")
        # transformation center: other
        self.OptionParser.add_option("--transform_center",
                                     action="store", type="string",
                                     dest="transform_center",
                                     default="bbox_anchor",
                                     help="Transformation center")
        # transformation center: custom
        self.OptionParser.add_option("--transform_cx",
                                     action="store", type="float",
                                     dest="transform_cx", default=0.0,
                                     help="Custom transformation center X")
        self.OptionParser.add_option("--transform_cy",
                                     action="store", type="float",
                                     dest="transform_cy", default=0.0,
                                     help="Custom transformation center Y")
        self.OptionParser.add_option("--transform_center_unit",
                                     action="store", type="string",
                                     dest="transform_center_unit",
                                     default="display",
                                     help="Unit for horizontal move")
        # copies
        self.OptionParser.add_option("--copy_mode",
                                     action="store", type="string",
                                     dest="copy_mode", default="drop",
                                     help="Copy mode")
        self.OptionParser.add_option("--copy_repeat",
                                     action="store", type="int",
                                     dest="copy_repeat", default=1,
                                     help="Number of copies")
        self.OptionParser.add_option("--apply_to_clone",
                                     action="store", type="inkbool",
                                     dest="apply_to_clone", default=False,
                                     help="Clone instead of copy")
        self.OptionParser.add_option("--clone_mode",
                                     action="store", type="string",
                                     dest="clone_mode", default="stamp",
                                     help="Clone mode")
        # shared options
        self.OptionParser.add_option("--apply_separately",
                                     action="store", type="inkbool",
                                     dest="apply_separately",
                                     default=False,
                                     help="Apply to each object separately")
        self.OptionParser.add_option("--apply_to_copy",
                                     action="store", type="inkbool",
                                     dest="apply_to_copy",
                                     default=False,
                                     help="Apply to copy")
        self.OptionParser.add_option("--visualize_refpoints",
                                     action="store", type="inkbool",
                                     dest="visualize_refpoints",
                                     default=False,
                                     help="Visualize position refpoint")
        # notebooks
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",  # default='"options_tab"',
                                     help="The selected transform tab")
        self.OptionParser.add_option("--transform_center_notebook",
                                     action="store", type="string",
                                     dest="transform_center_notebook",
                                     default='"transform_center_presets"',
                                     help="The selected transform center tab")

    # ----- utility methods

    def get_object_by_id(self, node_id):
        """Return SVG element with node_id."""
        if node_id in self.selected:
            return self.selected[node_id]
        else:
            return self.getElementById(node_id)

    # ----- compute bounding boxes of selection

    def get_bboxes(self, id_list, text_anchors=False):
        """Return dict with visual or geom bboxes of selected objects."""
        bboxes = {}
        if self.options.bbox_mode == 'visual':
            # visual bbox in root coordinates
            scale = self.unittouu('1px')
            offset = self.get_page_offset()
            root_id = self.document.getroot().get('id')
            query_ids = list(id_list)
            query_ids.append(root_id)
            bboxes = query_all(self.svg_file, query_ids, scale, offset)
            self.drawing_area = bboxes.pop(root_id)
        elif self.options.bbox_mode == 'geom':
            # geometric bbox in root coordinates
            for node_id in id_list:
                node = self.get_object_by_id(node_id)
                bbox = get_geom_bbox(node)
                if bbox is not None:
                    bboxes[node_id] = bbox
                elif text_anchors and is_regular_text(node):
                    bboxes[node_id] = get_text_pos(node, root=True)
                    bboxes[node_id].extend([0.0, 0.0])
            self.drawing_area = get_geom_bbox(self.document.getroot())
            if self.drawing_area is None:
                # if only text was selected, resort to query_all()
                scale = self.unittouu('1px')
                offset = self.get_page_offset()
                root_id = self.document.getroot().get('id')
                query_ids = [root_id]
                temp = query_all(self.svg_file, query_ids, scale, offset)
                self.drawing_area = temp.pop(root_id)
        return bboxes if bboxes else None

    def list_bboxes(self):
        """Verbose output of processed object bounding boxes."""
        if self.bboxes is not None:
            for k, v in self.bboxes.items():
                inkex.debug('{}: {}'.format(k, v))
        else:
            inkex.debug("No object bounding boxes processed.")

    def update_bboxes(self, node_id):
        """Update list of bbox values for node."""
        if self.bboxes is not None:
            if self.options.bbox_mode == 'geom':
                # replace values in dict
                node = self.getElementById(node_id)
                self.bboxes[node_id] = get_geom_bbox(node)
            else:
                # FIXME: manually "update" visual bboxes?  Rerunning external
                # process with query-all won't help unless a copy of the file
                # is saved in $TEMP first.
                pass

    def get_sorted_ids(self, sortby='position'):
        """Return node ids sorted by minx, miny coordinates."""

        def min_max_min_x(node_id):
            """Sort nodes from min to max X."""
            cx, cy = self.bboxes[node_id][0:2]
            return (cx, cy)

        def min_max_min_y(node_id):
            """Sort nodes from min to max Y."""
            cx, cy = self.bboxes[node_id][0:2]
            return (cy, cx)

        def min_max_mid_x(node_id):
            """Sort nodes from min to max X."""
            xmin, xmax = self.get_bbox_extents(node_id)[0:2]
            xmid = xmin + (xmax - xmin) / 2.0
            return (xmid, xmin, xmax)
            # return self.get_bbox_center(node_id)[0]

        def min_max_mid_y(node_id):
            """Sort nodes from min to max Y."""
            ymin, ymax = self.get_bbox_extents(node_id)[2:4]
            ymid = ymin + (ymax - ymin) / 2.0
            return (ymid, ymin, ymax)
            # return self.get_bbox_center(node_id)[1]

        def min_max_max_x(node_id):
            """Sort nodes from min to max X."""
            xmin, xmax = self.get_bbox_extents(node_id)[0:2]
            return (xmax, xmin)

        def min_max_max_y(node_id):
            """Sort nodes from min to max Y."""
            ymin, ymax = self.get_bbox_extents(node_id)[2:4]
            return (ymax, ymin)

        def min_max_width(node_id):
            """Sort nodes from min to max X."""
            width, height = self.bboxes[node_id][2:4]
            return (width, height)

        def min_max_height(node_id):
            """Sort nodes from min to max Y."""
            width, height = self.bboxes[node_id][2:4]
            return (height, width)

        sorted_ids = None

        if sortby == 'min' or sortby == 'position':
            sorted_ids = dict()
            sorted_ids['x'] = list(self.bboxes.keys())
            sorted_ids['y'] = list(sorted_ids['x'])
            sorted_ids['x'].sort(key=min_max_min_x, reverse=False)
            sorted_ids['y'].sort(key=min_max_min_y, reverse=False)

        elif sortby == 'mid':
            sorted_ids = dict()
            sorted_ids['x'] = list(self.bboxes.keys())
            sorted_ids['y'] = list(sorted_ids['x'])
            sorted_ids['x'].sort(key=min_max_mid_x, reverse=False)
            sorted_ids['y'].sort(key=min_max_mid_y, reverse=False)

        elif sortby == 'max':
            sorted_ids = dict()
            sorted_ids['x'] = list(self.bboxes.keys())
            sorted_ids['y'] = list(sorted_ids['x'])
            sorted_ids['x'].sort(key=min_max_max_x, reverse=False)
            sorted_ids['y'].sort(key=min_max_max_y, reverse=False)

        elif sortby == 'size':
            sorted_ids = dict()
            sorted_ids['x'] = list(self.bboxes.keys())
            sorted_ids['y'] = list(sorted_ids['x'])
            sorted_ids['x'].sort(key=min_max_width, reverse=False)
            sorted_ids['y'].sort(key=min_max_height, reverse=False)

        return sorted_ids

    # ----- selection bounding box

    def get_bboxes_extents(self):
        """Return extents of the combined bboxes."""
        if self.bboxes_extents is None:
            x, y, w, h = self.bboxes[list(self.bboxes.keys())[0]]
            extents = [x, x + w, y, y + h]
            for bbox in self.bboxes.values():
                x, y, w, h = bbox
                extents[0] = min(x, extents[0])
                extents[1] = max(x + w, extents[1])
                extents[2] = min(y, extents[2])
                extents[3] = max(y + h, extents[3])
            self.bboxes_extents = extents
        return self.bboxes_extents

    def get_bboxes_csp(self, center=False):
        """Return csp path for the combined bboxes."""
        bbox = bbox_extents_to_rect(self.get_bboxes_extents())
        return get_rect_csp(bbox, center)

    def get_bboxes_center(self):
        """Return center point of the combined bboxes."""
        if self.bboxes_center is None:
            extents = self.get_bboxes_extents()
            self.bboxes_center = [sum(extents[0:2]) / 2.0,
                                  sum(extents[2:4]) / 2.0]
        return self.bboxes_center

    def get_bboxes_anchor(self):
        """Return bounding box anchor point based on options."""
        bbox = bbox_extents_to_rect(self.get_bboxes_extents())
        ref = (self.options.bbox_anchor_x, self.options.bbox_anchor_y)
        coords = self.options.coord_system
        return get_rect_anchor(bbox, ref, coords)

    # ----- object bounding box

    def get_bbox_extents(self, node_id):
        """Return extents for bbox rect of node."""
        bbox = self.bboxes[node_id]
        return bbox_rect_to_extents(bbox)

    def get_bbox_csp(self, node_id, center=False):
        """Return csp path for bbox rect of node."""
        bbox = self.bboxes[node_id]
        return get_rect_csp(bbox, center)

    def get_bbox_center(self, node_id):
        """Return center point of bbox rect of node."""
        x, y, width, height = self.bboxes[node_id]
        return [x + width/2.0, y + height/2.0]

    def get_bbox_min(self, node_id):
        """Return min corner of bbox."""
        xmin, _, ymin, _ = self.get_bbox_extents(node_id)
        return [xmin, ymin]

    def get_bbox_mid(self, node_id):
        """Return mid point of bbox."""
        return self.get_bbox_center(node_id)

    def get_bbox_max(self, node_id):
        """Return max corner of bbox."""
        _, xmax, _, ymax = self.get_bbox_extents(node_id)
        return [xmax, ymax]

    def get_bbox_anchor(self, node_id):
        """Return bounding box anchor point based on options."""
        bbox = self.bboxes[node_id]
        ref = (self.options.bbox_anchor_x, self.options.bbox_anchor_y)
        coords = self.options.coord_system
        return get_rect_anchor(bbox, ref, coords)

    # ----- visualize a center

    def draw_cross(self, node_id, center, color="black"):
        """Draw a small cross to mark a transformation center."""
        node = inkex.etree.Element(inkex.addNS('path', 'svg'))
        node.set('id', 'TransformCenter_' + node_id)
        add_cross(node, center, scale=self.unittouu('1px'))
        sdict = simplestyle.parseStyle(node.get('style'))
        sdict['stroke'] = color
        node.set('style', simplestyle.formatStyle(sdict))
        self.document.getroot().append(node)

    def draw_x(self, node_id, center, color="black"):
        """Draw a small x to mark a transformation center."""
        node = inkex.etree.Element(inkex.addNS('path', 'svg'))
        node.set('id', 'TransformCenter_' + node_id)
        add_x(node, center, scale=self.unittouu('1px'))
        sdict = simplestyle.parseStyle(node.get('style'))
        sdict['stroke'] = color
        node.set('style', simplestyle.formatStyle(sdict))
        self.document.getroot().append(node)

    def draw_bbox(self, bbox):
        """Draw a rect visualizing the bounding box."""
        x, y, w, h = bbox
        node = inkex.etree.Element(inkex.addNS('rect', 'svg'))
        node.set('x', str(x))
        node.set('y', str(y))
        node.set('width', str(w))
        node.set('height', str(h))
        node.set('style', "fill:blue;fill-opacity:0.1;stroke:none")
        self.document.getroot().append(node)

    # ----- transformation center, angles, lengths

    def get_object_anchor(self, node_id, scope='transform'):
        """Return object anchor for selected object."""

        def opt_from_scope(name):
            """Get option with name *name* from *scope*."""
            return getattr(self.options, '{}_{}'.format(scope, name))

        object_ref = opt_from_scope('object_refpoint')
        if object_ref == 'inkscape':
            # inkscape transformation center per object
            node = self.get_object_by_id(node_id)
            center_bbox = self.get_bbox_center(node_id)
            anchor = get_inkscape_transform_center(node, center_bbox)
        elif object_ref == 'cspcofm':
            # cubicsuperpath center of mass (paths only)
            node = self.get_object_by_id(node_id)
            if is_path(node):
                csp = cubicsuperpath.parsePath(node.get('d'))
                mat = composeTransform(
                    get_mat_from_node_to_root(node.getparent()),
                    get_mat_of(node))
                applyTransformToPath(mat, csp)
                try:
                    anchor = cspcofm(csp)
                except AttributeError:
                    anchor = self.get_bbox_center(node_id)
            else:
                anchor = self.get_bbox_anchor(node_id)
        else:
            # bounding box reference point
            anchor = self.get_bbox_anchor(node_id)
        return anchor

    def get_transform_anchor(self, node_id, scope='transform'):
        """Return transformation center for selected object."""
        # pylint: disable=too-many-branches

        def opt_from_scope(name):
            """Get option with name *name* from *scope*."""
            return getattr(self.options, '{}_{}'.format(scope, name))

        anchor = [0.0, 0.0]  # fallback
        center_tab = tab_name(opt_from_scope('center_notebook'))
        if center_tab.endswith('center_bbox'):
            if self.options.apply_separately and node_id is not None:
                anchor = self.get_object_anchor(node_id, scope)
            else:
                anchor = self.get_bboxes_anchor()
        elif center_tab.endswith('center_presets'):
            center_preset = opt_from_scope('center')
            minx, miny, width, height = self.get_page_rect()
            anchor = 2 * [0.0]
            if center_preset.startswith('page_'):
                if 'bottom' in center_preset:
                    if 'mid' in center_preset:
                        anchor[0] = minx + (0.5 * width)
                    anchor[1] = miny + height
                if 'left' in center_preset:
                    if 'mid' in center_preset:
                        anchor[1] = miny + (0.5 * height)
                    anchor[0] = minx
                if 'right' in center_preset:
                    if 'mid' in center_preset:
                        anchor[1] = miny + (0.5 * height)
                    anchor[0] = minx + width
                if 'top' in center_preset:
                    if 'mid' in center_preset:
                        anchor[0] = minx + (0.5 * width)
                    anchor[1] = miny
                if 'center' in center_preset:
                    anchor[0] = minx + 0.5 * width
                    anchor[1] = miny + 0.5 * height
            elif center_preset == 'svg_origin':
                anchor = [0.0, 0.0]
            else:
                anchor = self.get_bboxes_anchor()
        elif center_tab.endswith('center_custom'):
            cx, cy = opt_from_scope('cx'), opt_from_scope('cy')
            c_unit = opt_from_scope('center_unit')
            if c_unit == 'uu':
                anchor = [cx, cy]
            elif c_unit == 'display':
                anchor = [self.convert_from_display_units(cx),
                          self.convert_from_display_units(cy)]
            else:
                anchor = [self.unittouu(str(cx) + c_unit),
                          self.unittouu(str(cy) + c_unit)]
            # dt to uu
            if self.options.coord_system == 'desktop':
                # TODO: take viewport offset into account
                page = self.get_page_size()
                anchor[1] = page[1] - anchor[1]
        return anchor

    def get_angle(self, scope='rotate_angle'):
        """Return angle in radians based on options."""

        def opt_from_scope(name=None):
            """Get option with name *name* from *scope*."""
            sname = scope + ('_{}'.format(name) if name is not None else '')
            return getattr(self.options, sname)

        angle = opt_from_scope()
        unit = opt_from_scope('unit')
        if unit == 'deg':
            return radians(angle)
        elif unit == 'grad':
            return radians(angle * 0.9)
        elif unit == 'turn':
            return radians(angle * 360.0)
        else:
            return angle

    def get_length(self, scope='tx'):
        """Return length in user units based on options."""

        def opt_from_scope(name=None):
            """Get option with name *name* from *scope*."""
            sname = scope + ('_{}'.format(name) if name is not None else '')
            return getattr(self.options, sname)

        length = opt_from_scope()
        unit = opt_from_scope('unit')
        if unit == 'percent':
            length /= 100.0
        elif unit == 'uu':
            pass
        elif unit == 'display':
            length = self.convert_from_display_units(length)
        else:
            length = self.unittouu(str(length) + unit)
        return length

    def check_repeat_transform(self, i=0):
        """Check whether to adjust transform for current copy/clone."""
        # pylint: disable=unused-argument
        check_for_copy = (self.options.apply_to_copy and
                          self.options.copy_mode != 'drop')
        check_for_clone = (self.options.apply_to_copy and
                           self.options.apply_to_clone)
        return check_for_copy or check_for_clone

    def draw_refpoints(self, node, mat, bbox=False):
        """Visualize the reference points of object and transformation."""
        node_id = node.get('id')
        mat_node = get_mat_composed_in_node(node, None, mat)
        center_orig = self.get_object_anchor(node_id)
        center_new = get_transformed_point(mat_node, center_orig)
        if bbox:
            self.draw_bbox(self.bboxes[node_id])
        self.draw_cross(node_id+"_orig", center_orig, color="blue")
        self.draw_cross(node_id+"_new", center_new, color="red")

    # ----- transformation methods

    def transform(self, mat, node, update=True):
        """Apply final transformation matrix to object, or a copy/clone."""
        if self.options.visualize_refpoints:
            self.draw_refpoints(node, mat)
        node_id = node.get('id')
        if self.options.apply_to_copy:
            if self.options.apply_to_clone:
                copy_node = make_clone(node, self.options.clone_mode)
            else:
                copy_node = make_copy(node, self.options.copy_mode)
            applyTransformToNode(mat, copy_node)
        else:
            applyTransformToNode(mat, node)
        if update and self.options.apply_to_copy:
            self.update_bboxes(node_id)

    def stretch(self, node_id):
        """Stretch object with node_id to extend to target."""
        # pylint: disable=too-many-locals
        node = self.get_object_by_id(node_id)
        # get selection extents (cached)
        s_xmin, s_xmax, s_ymin, s_ymax = self.get_bboxes_extents()
        # get node bbox extents
        n_xmin, n_xmax, n_ymin, n_ymax = self.get_bbox_extents(node_id)
        # option
        target = self.options.stretch_target
        # compute required scale factor and anchor
        anchor = None
        sx = sy = 1.0
        if target == 'xmin':
            sx, anchor = stretch_factor(n_xmin > s_xmin, n_xmax - n_xmin,
                                        n_xmax - s_xmin, [n_xmax, 0.0])
        elif target == 'xmax':
            sx, anchor = stretch_factor(n_xmax < s_xmax, n_xmax - n_xmin,
                                        s_xmax - n_xmin, [n_xmin, 0.0])
        elif target == 'ymin':
            sy, anchor = stretch_factor(n_ymin > s_ymin, n_ymax - n_ymin,
                                        n_ymax - s_ymin, [0.0, n_ymax])
        elif target == 'ymax':
            sy, anchor = stretch_factor(n_ymax < s_ymax, n_ymax - n_ymin,
                                        s_ymax - n_ymin, [0.0, n_ymin])
        if anchor is not None:
            mat_stretch = [[sx, 0.0, 0.0],
                           [0.0, sy, 0.0]]
            # transform to root, reflect, transform back to node
            mat = get_mat_composed_in_root(node, anchor, mat_stretch)
            self.transform(mat, node, update=False)

    def reflect(self, line, node_id):
        """Reflect object with node_id across mirror line."""
        node = self.get_object_by_id(node_id)
        # mirror line
        phi = get_reflect_angle_x(*line)
        if phi:
            tx = get_reflect_intercept_x(*line)
            ty = 0 if not isnan(tx) else None
        else:
            ty = get_reflect_intercept_y(*line)
            tx = 0 if not isnan(ty) else None
        if tx is None or ty is None:
            # point reflection
            anchor = line[0]
            mat_mirror = [[-1.0, 0.0, 0.0],
                          [0.0, -1.0, 0.0]]
        else:
            # line reflection
            anchor = [tx, ty]
            mat_mirror = [[cos(2*phi), sin(2*phi), 0.0],
                          [sin(2*phi), -cos(2*phi), 0.0]]
        # transform to root, reflect, transform back to node
        mat = get_mat_composed_in_root(node, anchor, mat_mirror)
        self.transform(mat, node, update=False)

    def matrix(self, i, node_id):
        """Matrix-transform object with node_id based on options."""
        node = self.get_object_by_id(node_id)
        # matrix values
        a = self.options.matrix_a
        b = self.options.matrix_b
        c = self.options.matrix_c
        d = self.options.matrix_d
        e = self.options.matrix_e
        f = self.options.matrix_f
        # transformation anchor
        anchor = self.get_transform_anchor(node_id)
        # transform to root, apply matrix, transform back to node
        mat_matrix = [[a, c, e],
                      [b, d, f]]
        # copies
        if self.check_repeat_transform(i):
            mat_one = copy.deepcopy(mat_matrix)
            for _ in range(i):
                mat_matrix = composeTransform(mat_matrix, mat_one)
        mat = get_mat_composed_in_root(node, anchor, mat_matrix)
        self.transform(mat, node)

    def skew(self, i, node_id):
        """Skew object with node_id based on options."""
        # pylint: disable=too-many-branches
        node = self.get_object_by_id(node_id)
        # bounding box
        if self.options.apply_separately:
            bbox = self.bboxes[node_id]
        else:
            bbox = bbox_extents_to_rect(self.get_bboxes_extents())
        # skew value for x direction
        # Examples for a horizontal skew of a 100 by 50 px bounding box with
        # the Rotation center in the middle of the bounding box: ...
        if self.options.skew_x_unit in ANGULAR_UNITS:
            # Angle of 30deg: The top edge of the box is moved 14.4 px
            # (tan(30deg) * 50 px * 0.5) to the left (angles are defined to be
            # positive in the counterclockwise direction) ...
            skew_x = tan(self.get_angle('skew_x'))
        else:
            delta_x = self.get_length('skew_x')
            if self.options.skew_x_unit == 'percent':
                # Percentage of 20%: The top edge is moved 5 px to the right
                # (half of 20% of the height) to the right ...
                skew_x = ((delta_x * bbox[3]) / 2.0) / -(bbox[3] / 2.0)
            else:
                # Distance of 20 px: The top edge of the box is moved 10 px
                # (half of 20 px) to the right ...
                skew_x = (delta_x / 2.0) / -(bbox[3] / 2.0)
        # skew value for y direction
        if self.options.skew_y_unit in ANGULAR_UNITS:
            skew_y = tan(self.get_angle('skew_y')) * -1
        else:
            delta_y = self.get_length('skew_y')
            if self.options.coord_system == 'desktop':
                delta_y *= -1
            if self.options.skew_y_unit == 'percent':
                skew_y = ((delta_y * bbox[2]) / 2.0) / (bbox[2] / 2.0)
            else:
                skew_y = (delta_y / 2.0) / (bbox[2] / 2.0)
        # transformation anchor
        anchor = self.get_transform_anchor(node_id)
        # transform to root, skew, transform back to node
        mat_skew = [[1.0, skew_x, 0.0],
                    [skew_y, 1.0, 0.0]]
        # copies
        if self.check_repeat_transform(i):
            mat_one = copy.deepcopy(mat_skew)
            for _ in range(i):
                mat_skew = composeTransform(mat_skew, mat_one)
        mat = get_mat_composed_in_root(node, anchor, mat_skew)
        self.transform(mat, node)

    def rotate(self, i, node_id):
        """Rotate object with node_id based on options."""
        node = self.get_object_by_id(node_id)
        # rotation angle
        angle = self.get_angle('rotate_angle')
        # rotation direction
        if self.options.rotate_angle_direction == 'ccw':
            angle *= -1
        # transformation anchor
        anchor = self.get_transform_anchor(node_id)
        # copies
        if self.check_repeat_transform(i):
            # increase rotate factor for copies
            angle *= i + 1
        # transform to root, rotate, transform back to node
        mat_rotate = [[cos(angle), -sin(angle), 0.0],
                      [sin(angle), cos(angle), 0.0]]
        mat = get_mat_composed_in_root(node, anchor, mat_rotate)
        self.transform(mat, node)

    def scale(self, i, node_id):
        """Scale object with node_id based on options."""
        # pylint: disable=too-many-locals
        node = self.get_object_by_id(node_id)
        # bounding box
        if self.options.apply_separately:
            bbox = self.bboxes[node_id]
        else:
            bbox = bbox_extents_to_rect(self.get_bboxes_extents())
        # scale factor x based on width
        sx = self.get_length(scope="sx")
        if self.options.sx_unit != 'percent':
            sx /= bbox[2]
        # scale factor y based on height
        if self.options.scale_proportionally:
            # lock ratio to x scale factor
            sy = sx
        else:
            sy = self.get_length(scope="sy")
            if self.options.sy_unit != 'percent':
                sy /= bbox[3]
        # transformation anchor
        anchor = self.get_transform_anchor(node_id)
        # copies
        if self.check_repeat_transform(i):
            # increase scale factor for copies
            sx = pow(sx, i + 1)
            sy = pow(sy, i + 1)
        # transform to root, scale, transform back to node
        mat_scale = [[sx, 0.0, 0.0],
                     [0.0, sy, 0.0]]
        mat = get_mat_composed_in_root(node, anchor, mat_scale)
        if self.options.scale_position_only:
            # compute offset of object centerpoint after scaling
            mat_node = get_mat_composed_in_node(node, None, mat)
            center_orig = self.get_object_anchor(node_id)
            center_new = get_transformed_point(mat_node, center_orig)
            tx, ty = [v2 - v1 for v1, v2 in zip(center_orig, center_new)]
            # apply offset as translation instead of scaling
            mat_position = [[1.0, 0.0, tx],
                            [0.0, 1.0, ty]]
            mat = get_mat_composed_in_root(node, None, mat_position)
        self.transform(mat, node)

    def translate_polar(self, i, node_id):
        """Translate selected objects based on polar coordinates."""
        node = self.get_object_by_id(node_id)
        # offset
        distance = self.get_length(scope="polar_distance")
        angle = self.get_angle(scope="polar_angle")
        # options
        if self.check_repeat_transform(i):
            if self.options.polar_distance_unit == 'percent':
                # increase scale factor for copies
                distance = pow(distance, i + 1)
            else:
                # increase offset factor for copies
                distance *= i + 1
            if not self.options.apply_separately:
                if self.options.polar_angle_relative:
                    angle *= i + 1
        if self.options.polar_angle_direction == 'ccw':
            angle *= -1
        # reference points
        center = Point(*self.get_transform_anchor(node_id))
        anchor = Point(*self.get_object_anchor(node_id))
        # delta radius and angle
        delta = anchor - center
        if isclose(delta.r, 0, abs_tol=1e-03):
            # check for coinciding points to avoid "random" angles
            delta.r = 0.0
        if self.options.polar_distance_unit == 'percent':
            delta.r *= distance
        elif self.options.polar_distance_relative:
            delta.r += distance
        else:
            delta.r = distance
        if self.options.polar_angle_relative:
            delta.t += angle
        else:
            delta.t = angle
        # new reference point
        anchor_new = center + delta
        tx, ty = list(anchor_new - anchor)
        # transform to root, translate, transform back to node
        mat_translate = [[1.0, 0.0, tx],
                         [0.0, 1.0, ty]]
        mat = get_mat_composed_in_root(node, list(center), mat_translate)
        self.transform(mat, node)

    def translate_cartesian(self, i, node_id, sorted_ids=None):
        """Translate object with node_id based on options."""
        node = self.get_object_by_id(node_id)
        page = self.get_page_size()
        # spacing out
        factor_x = factor_y = 1
        if self.options.apply_separately and sorted_ids is not None:
            if self.options.tx_relative:
                factor_x = sorted_ids['x'].index(node_id) + 1
            if self.options.ty_relative:
                factor_y = sorted_ids['y'].index(node_id) + 1
        # offset
        tx = self.get_length(scope="tx")
        ty = self.get_length(scope="ty")
        # relative / absolute move
        anchor = self.get_bboxes_anchor()
        # horizontal
        if self.options.tx_relative:
            pass
        else:
            tx -= anchor[0]
        # vertical
        if self.options.ty_relative:
            if self.options.coord_system == 'desktop':
                ty *= -1  # dt to uu
        else:
            if self.options.coord_system == 'desktop':
                ty = page[1] - ty  # dt to uu
            ty -= anchor[1]
        # translate transform itself has no anchor
        anchor = None
        # copies
        if self.check_repeat_transform(i):
            # increase offset factor for copies
            factor_x *= i + 1
            factor_y *= i + 1
        # transform to root, translate, transform back to node
        mat_translate = [[1.0, 0.0, factor_x * tx],
                         [0.0, 1.0, factor_y * ty]]
        mat = get_mat_composed_in_root(node, anchor, mat_translate)
        self.transform(mat, node)

    def demo(self):
        """Main entry for demo displaying list of processed bboxes."""
        # get z-sorted list of selected objects
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        # cache dict with visual or geometrix bbox values
        self.bboxes = self.get_bboxes(id_list)
        # actions
        if self.bboxes is not None and len(self.bboxes) == len(id_list):
            self.list_bboxes()

    def effect(self):
        """Main entry point to process current document."""
        self.demo()


class TransformObjects(TransformBasic):
    """Effect-based class to transform selected objects."""

    def __init__(self):
        """Init base class."""
        TransformBasic.__init__(self)

        # notebooks
        self.OptionParser.add_option("--transform_notebook",
                                     action="store", type="string",
                                     dest="transform_notebook",
                                     default='"translate_tab"',
                                     help="The selected transform tab")
        self.OptionParser.add_option("--translate_notebook",
                                     action="store", type="string",
                                     dest="translate_notebook",
                                     default='"translate_cartesian_tab"',
                                     help="The selected transform tab")
        # dispatcher
        self.OptionParser.add_option("--extension",
                                     action="store", type="string",
                                     dest="extension",
                                     default="transform_objects",
                                     help="[hidden] extension parameter")

    # ----- stretch each selected object to min or max extent

    def stretch_objects(self):
        """Main entry for 'Stretch Objects' extension."""
        # set options required to stretch objects to selection edge
        self.options.apply_separately = True
        self.options.apply_to_copy = False
        self.options.visualize_refpoints = False
        self.options.sx_unit = "uu"
        self.options.sy_unit = "uu"
        self.options.scale_proportionally = False
        self.options.scale_position_only = False
        # get z-sorted list of selected objects
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        # cache dict with visual or geometrix bbox values
        self.bboxes = self.get_bboxes(id_list)
        # actions
        if self.bboxes is not None and len(self.bboxes) == len(id_list):
            for node_id in id_list:
                self.stretch(node_id)

    # ----- rotate selection as a group

    def rotate_objects(self):
        """Main entry for rotate extensions without GUI."""
        # set options required to rotate selected objects as a group
        self.options.transform_notebook = '"scale_tab"'
        self.options.apply_separately = False
        self.options.apply_to_copy = False
        self.options.visualize_refpoints = False
        self.options.rotate_angle_unit = "deg"
        self.options.coord_system = "desktop"
        self.options.bbox_mode = "geom"
        self.options.transform_center_notebook = '"transform_center_bbox"'
        self.options.transform_object_refpoint = "bbox"
        self.options.bbox_anchor_x = "midx"
        self.options.bbox_anchor_y = "midy"
        # get z-sorted list of selected objects
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        # cache dict with visual or geometric bbox values
        self.bboxes = self.get_bboxes(id_list)
        # actions
        if self.bboxes is not None and len(self.bboxes) == len(id_list):
            for node_id in id_list:
                self.rotate(0, node_id)

    def rotate_objects_90_ccw(self):
        """Main entry for 'Rotate 90 deg CCW' extension."""
        # rotation angle and direction
        self.options.rotate_angle = 90
        self.options.rotate_angle_direction = "ccw"
        self.rotate_objects()

    def rotate_objects_90_cw(self):
        """Main entry for 'Rotate 90 deg CW' extension."""
        # rotation angle and direction
        self.options.rotate_angle = 90
        self.options.rotate_angle_direction = "cw"
        self.rotate_objects()

    # ----- flip objects individually

    def flip_objects(self):
        """Main entry for flip extensions without GUI."""
        # set options required to flip objects in selection individually
        self.options.transform_notebook = '"scale_tab"'
        self.options.apply_separately = True
        self.options.apply_to_copy = False
        self.options.visualize_refpoints = False
        self.options.sx_unit = "percent"
        self.options.sy_unit = "percent"
        self.options.scale_proportionally = False
        self.options.scale_position_only = False
        self.options.coord_system = "desktop"
        self.options.bbox_mode = "geom"
        self.options.transform_center_notebook = '"transform_center_bbox"'
        self.options.transform_object_refpoint = "bbox"
        self.options.bbox_anchor_x = "midx"
        self.options.bbox_anchor_y = "midy"
        # get z-sorted list of selected objects
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        # cache dict with visual or geometrix bbox values
        self.bboxes = self.get_bboxes(id_list)
        # actions
        if self.bboxes is not None and len(self.bboxes) == len(id_list):
            for node_id in id_list:
                self.scale(0, node_id)

    def flip_objects_horizontal(self):
        """Main entry for 'Flip Horizontal' extension."""
        # scale factors
        self.options.sx = -100
        self.options.sy = 100
        self.flip_objects()

    def flip_objects_vertical(self):
        """Main entry for 'Flip Vertical' extension."""
        # scale factors
        self.options.sx = 100
        self.options.sy = -100
        self.flip_objects()

    # ----- mirror objects

    def mirror_objects(self):
        """Main entry for 'Mirror' extension."""
        self.options.visualize_refpoints = False
        id_list = z_sort(self.document.getroot(), self.selected.keys())
        if id_list is not None and len(id_list) > 1:
            top_path = self.selected[id_list.pop(-1)]
            if is_path(top_path):
                line = get_reflect_points(top_path)
                if line is not None:
                    for node_id in id_list:
                        self.reflect(line, node_id)
                else:
                    inkex.debug("Topmost selected element is not a valid "
                                "mirror line. Try changing z-order of the "
                                "selected objects first, or draw a new mirror "
                                "line as topmost element.")
            else:
                inkex.debug("Topmost selected element is not a path - "
                            "check the z-order of the selected objects first, "
                            "or convert the topmost object to path.")
        else:
            inkex.debug("This extension requires "
                        "two or more selected objects.")

    # ----- transform polar

    def polar_transform(self):
        """Main entry for 'Transform Polar' extension."""
        # check for unsupported combinations of options
        if not self.check_options():
            return
        # get z-sorted list of selected objects
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        # cache dict with visual or geometrix bbox values
        self.bboxes = self.get_bboxes(id_list)
        # actions
        if self.bboxes is not None and len(self.bboxes) == len(id_list):
            transform_action = tab_name(self.options.transform_notebook)
            # visualize transformation center
            if self.options.visualize_refpoints:
                anchor = self.get_transform_anchor(None)
                self.draw_x("main", anchor, color="black")
            # copies
            if self.options.apply_to_copy:
                repeat_range = list(reversed(range(self.options.copy_repeat)))
            else:
                repeat_range = [0]
            # selection
            for node_id in id_list:
                for i in repeat_range:
                    if transform_action == 'polar_move_tab':
                        self.translate_polar(i, node_id)

    # ----- transform objects

    def transform_objects(self):
        """Main entry for 'Transform Objects' extension."""
        # pylint: disable=too-many-branches
        # check for unsupported combinations of options
        if not self.check_options():
            return
        # get z-sorted list of selected objects
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        # cache dict with visual or geometrix bbox values
        self.bboxes = self.get_bboxes(id_list)
        # actions
        if self.bboxes is not None and len(self.bboxes) == len(id_list):
            transform_action = tab_name(self.options.transform_notebook)
            # visualize transformation center
            if self.options.visualize_refpoints:
                anchor = self.get_transform_anchor(None)
                self.draw_x("main", anchor, color="black")
            # sorted ids for spacing out option
            if transform_action == 'translate_tab':
                if self.options.apply_separately:
                    sorted_ids = self.get_sorted_ids(sortby='position')
                else:
                    sorted_ids = None
            # copies
            if self.options.apply_to_copy:
                repeat_range = list(reversed(range(self.options.copy_repeat)))
            else:
                repeat_range = [0]
            # selection
            for node_id in id_list:
                for i in repeat_range:
                    if transform_action == 'translate_tab':
                        translate_action = tab_name(
                            self.options.translate_notebook)
                        if translate_action == 'translate_cartesian_tab':
                            self.translate_cartesian(i, node_id, sorted_ids)
                        elif translate_action == 'translate_polar_tab':
                            self.translate_polar(i, node_id)
                    elif transform_action == 'scale_tab':
                        self.scale(i, node_id)
                    elif transform_action == 'rotate_tab':
                        self.rotate(i, node_id)
                    elif transform_action == 'skew_tab':
                        self.skew(i, node_id)
                    elif transform_action == 'matrix_tab':
                        self.matrix(i, node_id)

    # ----- main

    def check_options(self):
        """Check options for unsupported combinations."""
        msgs = []
        # add checks here
        if (tab_name(self.options.transform_notebook) == 'scale_tab' and
                # check for scale position only, visual bbox, multiple copies
                self.options.scale_position_only and
                self.options.apply_to_copy and
                self.options.copy_repeat > 1 and
                self.options.bbox_mode == 'visual'):
            msgs.append("Unsupported combination of options. "
                        "If you need more than one copy-clones in "
                        "'Scale positions only' mode, "
                        "try switching bbox_mode from visual to geometric. ")
        if not len(msgs):
            return True
        else:
            for msg in msgs:
                inkex.debug(msg)

    def effect(self):
        """Main entry point to process current document."""
        func = getattr(self, self.options.extension, None)
        if func is not None:
            return func()
        else:
            inkex.debug('{} not found.'.format(self.options.extension))


if __name__ == '__main__':
    ME = TransformObjects()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
