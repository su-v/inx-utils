#!/usr/bin/env python
"""
reset_ids - reset ids of selected objects
            (delete attribute, let Inkscape generate new ids on reload)

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors

# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex


__version__ = '0.0'


def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


def is_group(node):
    """Check whether node is SVG container element 'g'."""
    return node.tag == inkex.addNS('g', 'svg')


def is_layer(node):
    """Check whether node is inkscape layer group."""
    return (is_group(node) and
            node.get(inkex.addNS('groupmode', 'inkscape')) == "layer")


def set_layer(node):
    """Convert group to layer."""
    if is_group(node):
        label = 'Grouplayer {}'.format(node.get('id'))
        node.set(inkex.addNS('groupmode', 'inkscape'), "layer")
        node.set(inkex.addNS('label', 'inkscape'), label)


def unset_layer(node):
    """Convert layer to group."""
    if is_layer(node):
        node.attrib.pop(inkex.addNS('groupmode', 'inkscape'), None)


def create_group():
    """Return new SVG group element."""
    return inkex.etree.Element(inkex.addNS('g', 'svg'))


def reset_id(node):
    """Remove id attribute from node."""
    return node.attrib.pop('id', None)


class ResetIds(inkex.Effect):
    """Effect-based class to reset IDs of selected objects."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # options
        self.OptionParser.add_option("--action",
                                     action="store", type="string",
                                     dest="action",
                                     default="reset_selected",
                                     help="Selected action")

    def reset_ids(self, id_list):
        """Reset (remove) id attributes of selected objects."""
        for node_id in reversed(id_list):
            reset_id(self.getElementById(node_id))

    def process_selection(self, id_list):
        """Recursively process nested groups in selection."""
        for node_id in reversed(id_list):
            node = self.getElementById(node_id)
            if is_group(node):
                child_list = [child.get('id') for child in node]
                self.process_selection(child_list)
            reset_id(node)

    def effect(self):
        """Main entry point to process current document."""
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        if self.options.action == 'reset_selected':
            self.reset_ids(id_list)
        elif self.options.action == 'reset_selected_recursively':
            self.process_selection(id_list)


if __name__ == '__main__':
    ME = ResetIds()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
