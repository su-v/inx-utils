#!/usr/bin/env python
"""
transformation_center - set position of transformation center numerically

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Request:
    - lp200720: Set transform center numerically


Notes:
    - reset
    - set x, y independently
    - set relative to bbox ref / in absolute coords
    - set relative or absolute coords for multiple selected objects
    - support units (uu, desktop, physical, percentage of bbox size)
    - support desktop and svg coord system
    - display current center, coords (stderr, or on-canvas)


"""
# pylint: disable=too-many-ancestors
# pylint: disable=too-many-lines

# standard library
import csv
from subprocess import Popen, PIPE
import timeit

# inkscape library
try:
    import inkex_local as inkex
except ImportError:
    import inkex
import simplestyle
import simpletransform

# local library
from transform_objects import EffectCompat, invertTransform


__version__ = '0.0'


DEBUG = False


# ----- general helper functions

def timed(f):
    """Minimalistic timer for functions."""
    # pylint: disable=invalid-name
    start = timeit.default_timer()
    ret = f()
    elapsed = timeit.default_timer() - start
    return ret, elapsed


def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    """Test approximate equality.

    ref:
        PEP 485 -- A Function for testing approximate equality
        https://www.python.org/dev/peps/pep-0485/#proposed-implementation
    """
    # pylint: disable=invalid-name
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


def clamp(val, val_min, val_max):
    """Clamp value to min/max values."""
    return max(val_min, min(val, val_max))


def interpolate(t, a, b):
    """Interpolate interval defined by a and by at t."""
    return ((1.0 - t) * a) + (t * b)


def parameterize(x, a, b):
    """Parameterize x to interval defined by a and b."""
    if a == b:
        raise ValueError("Equal values a, b do not define an interval for x.")
    return (x - a) / (b - a)


def formatted(f, precision=8):
    """Pretty-printer for floats, return formatted string."""
    # pylint: disable=invalid-name
    fstring = '.{0}f'.format(precision)
    return format(f, fstring).rstrip('0').rstrip('.')


# ----- utility functions for SVG elements

def is_flowed_text(node):
    """Check whether node is <flowRoot> type."""
    return node.tag == inkex.addNS('flowRoot', 'svg')


def is_regular_text(node):
    """Check whether node is <text> or <flowRoot> type."""
    return node.tag == inkex.addNS('text', 'svg')


def is_text(node):
    """Check whether node is <text> or <flowRoot> type."""
    return is_regular_text(node) or is_flowed_text(node)


# ----- query objects

def run_inkquery(command_format, stdin_str=None, verbose=False):
    """Run command"""
    if verbose:
        inkex.debug(command_format)
    out = err = None
    myproc = Popen(command_format, shell=False,
                   stdin=PIPE, stdout=PIPE, stderr=PIPE,
                   universal_newlines=True)
    out, err = myproc.communicate(stdin_str)
    if myproc.returncode == 0:
        return out
    elif err is not None:
        inkex.errormsg(err)


def query_all(svgfile, id_list=None, scale=1.0, offset=None):
    """Spawn external inkscape to query all objects in *svgfile*."""
    opts = ['inkscape']
    opts.append('--query-all')
    opts.append(svgfile)
    stdout_str = run_inkquery(opts, stdin_str=None, verbose=False)
    reader = csv.reader(stdout_str.splitlines())
    positions = {}
    for line in reader:
        if len(line) and (id_list is None or line[0] in id_list):
            positions[line[0]] = [scale * float(v) for v in line[1:]]
            if offset is not None and len(offset) == 2:
                for i in range(2):
                    positions[line[0]][i] += offset[i]
    return positions


# ----- extended simpletransform.py

# pylint: disable=invalid-name
# pylint: disable=missing-docstring

def ident_mat():
    return [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]


def rootToNodeTransform(node, mat=None):
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        return invertTransform(
            simpletransform.composeParents(node, mat))
    else:
        return mat


def nodeToRootTransform(node, mat=None):
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        return simpletransform.composeParents(node, mat)
    else:
        return mat


def fromToTransform(node1, node2, mat=None):
    mat = ident_mat() if mat is None else mat
    return simpletransform.composeTransform(nodeToRootTransform(node1, mat),
                                            rootToNodeTransform(node2, mat))


def computePointInNode(pt, node, mat=None):
    point = list(pt)
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        simpletransform.applyTransformToPoint(
            rootToNodeTransform(node, mat), point)
    return point


def computePointInRoot(pt, node, mat=None):
    point = list(pt)
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        simpletransform.applyTransformToPoint(
            nodeToRootTransform(node, mat), point)
    return point

# pylint: enable=invalid-name
# pylint: enable=missing-docstring


# ----- SVG helper functions

def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


def is_rect(node):
    """Check whether node is SVG element type <rect/>."""
    return node.tag == inkex.addNS('rect', 'svg')


def bbox_extents_to_rect(extents):
    """Return bbox extents converted to rect data (x, y, w, h)."""
    minx, maxx, miny, maxy = extents
    return [minx, miny, maxx - minx, maxy - miny]


def bbox_rect_to_extents(bbox):
    """Return bbox rect converted to extents (minx, maxx, miny, maxy)."""
    minx, miny, width, height = bbox
    return [minx, minx + width, miny, miny + height]


def geom_bbox(node):
    """Return geometric bbox values of node in root coordinates."""
    node_mat = nodeToRootTransform(node.getparent())
    bbox_extents = simpletransform.computeBBox([node], node_mat)
    if bbox_extents is not None and len(bbox_extents) == 4:
        return bbox_extents_to_rect(bbox_extents)


# ----- draw helper functions

def bbox_default_style():
    """Return default style for visualized bbox shape or path."""
    return "fill:blue;fill-opacity:0.25;stroke:none"


def draw_bbox_path(bbox):
    """Draw path with bbox position and dimensions."""
    x, y, width, height = bbox
    path_d = ""
    path_d += "m {},{} ".format(x, y)
    path_d += "h {} ".format(width)
    path_d += "v {} ".format(height)
    path_d += "h {} ".format(-width)
    path_d += "z"
    bbox_path = inkex.etree.Element(inkex.addNS('path', 'svg'))
    bbox_path.set('d', path_d)
    bbox_path.set('style', bbox_default_style())
    return bbox_path


def draw_bbox_poly(bbox, closed=False):
    """Draw polyline or polygon with bbox position and dimensions."""
    x, y, width, height = bbox
    points = ""
    points += "{},{} ".format(x, y)
    points += "{},{} ".format(x + width, y)
    points += "{},{} ".format(x + width, y + height)
    points += "{},{} ".format(x, y + height)
    if closed:
        bbox_poly = inkex.etree.Element(inkex.addNS('polygon', 'svg'))
    else:
        bbox_poly = inkex.etree.Element(inkex.addNS('polyline', 'svg'))
    bbox_poly.set('points', points.strip())
    bbox_poly.set('style', bbox_default_style())
    return bbox_poly


def draw_bbox_rect(bbox):
    """Draw rect with bbox position and dimensions."""
    x, y, width, height = bbox
    bbox_rect = inkex.etree.Element(inkex.addNS('rect', 'svg'))
    bbox_rect.set('x', str(x))
    bbox_rect.set('y', str(y))
    bbox_rect.set('width', str(width))
    bbox_rect.set('height', str(height))
    bbox_rect.set('style', bbox_default_style())
    return bbox_rect


# ----- markers

def add_cross(node, center, radius=3.0, scale=1.0, snap_target="intersection"):
    """Add path data for cross to node."""
    x, y = center
    radius *= scale
    path_d = ''
    # horizontal diameter
    path_d += 'm {},{} '.format(x-radius, y)
    if snap_target == 'intersection':
        path_d += 'h {} '.format(2*radius)
    else:  # if snap_target == 'smooth_center':
        path_d += 'h {} '.format(radius)
        path_d += 'h {} '.format(radius)
    # vertical diameter
    path_d += 'm {},{} '.format(-radius, -radius)
    if snap_target == 'intersection':
        path_d += 'v {} '.format(2*radius)
    else:  # if snap_target == 'smooth_center':
        path_d += 'v {} '.format(radius)
        path_d += 'v {} '.format(radius)
    node.set('d', path_d)
    sdict = {
        'fill': 'none',
        'stroke': 'black',
        'stroke-width': str(0.5 * scale),
    }
    node.set('style', simplestyle.formatStyle(sdict))


def add_x(node, center, radius=3.0, scale=1.0, snap_target="intersection"):
    """Add path data for cross to node."""
    x, y = center
    radius *= scale
    path_d = ''
    # first diagonal
    path_d += 'm {},{} '.format(x-radius, y-radius)
    if snap_target == 'intersection':
        path_d += 'l {},{} '.format(2*radius, 2*radius)
    else:  # if snap_target == 'smooth_center':
        path_d += 'l {},{} '.format(radius, radius)
        path_d += 'l {},{} '.format(radius, radius)
    # second diagonal
    path_d += 'm 0,{} '.format(-2*radius)
    if snap_target == 'intersection':
        path_d += 'l {},{} '.format(-2*radius, 2*radius)
    else:  # if snap_target == 'smooth_center':
        path_d += 'l {},{} '.format(-radius, radius)
        path_d += 'l {},{} '.format(-radius, radius)
    node.set('d', path_d)
    sdict = {
        'fill': 'none',
        'stroke': 'black',
        'stroke-width': str(0.5 * scale),
    }
    node.set('style', simplestyle.formatStyle(sdict))


# ----- Inkscape transformation center

def has_inkscape_transform_center(node):
    """Check if node has Inkscape transformation center defined."""
    return (inkex.addNS('transform-center-x', 'inkscape') in node.attrib or
            inkex.addNS('transform-center-y', 'inkscape') in node.attrib)


def get_inkscape_transform_center(node, bbox_center):
    """Return coordinates of Inkscape's transform center."""
    dx = node.get(inkex.addNS('transform-center-x', 'inkscape'), 0)
    dy = node.get(inkex.addNS('transform-center-y', 'inkscape'), 0)
    return [bbox_center[0] + float(dx),
            bbox_center[1] - float(dy)]


def set_inkscape_transform_center(node, bbox_center, point):
    """Set coordinates of Inkscape's transform center.

    Parameter *point* is defined in SVG coordinates with user units."""
    for i, prop in enumerate(['transform-center-x', 'transform-center-y']):
        if point[i] is None:
            node.attrib.pop(inkex.addNS(prop, 'inkscape'), None)
        else:
            delta = [1, -1][i] * (point[i] - bbox_center[i])
            node.set(inkex.addNS(prop, 'inkscape'), str(delta))
    return get_inkscape_transform_center(node, bbox_center)


# ------ main class

class TransformCenter(EffectCompat):
    """Effect-based class to set transformation centers numerically."""
    # pylint: disable=too-many-public-methods

    def __init__(self):
        """Init base class."""
        EffectCompat.__init__(self)

        # instance attributes
        self.bboxes = None

        # options
        self.OptionParser.add_option("--action",
                                     action="store", type="string",
                                     dest="action",
                                     default="transformation_center",
                                     help="[hidden] Selected action")
        # position
        self.OptionParser.add_option("--coord_x",
                                     action="store", type="float",
                                     dest="coord_x",
                                     default=0.0,
                                     help="Value for x coordinate")
        self.OptionParser.add_option("--coord_x_unit",
                                     action="store", type="string",
                                     dest="coord_x_unit",
                                     default="display",
                                     help="Unit for x coordinate")
        self.OptionParser.add_option("--coord_x_object_ref",
                                     action="store", type="string",
                                     dest="coord_x_object_ref",
                                     default="origin",
                                     help="Refpoint for x coordinate")
        self.OptionParser.add_option("--coord_y",
                                     action="store", type="float",
                                     dest="coord_y",
                                     default=0.0,
                                     help="Value for y coordinate")
        self.OptionParser.add_option("--coord_y_unit",
                                     action="store", type="string",
                                     dest="coord_y_unit",
                                     default="display",
                                     help="Unit for y coordinate")
        self.OptionParser.add_option("--coord_y_object_ref",
                                     action="store", type="string",
                                     dest="coord_y_object_ref",
                                     default="origin",
                                     help="Refpoint for y coordinate")
        # common options
        self.OptionParser.add_option("--bbox_mode",
                                     action="store", type="string",
                                     dest="bbox_mode",
                                     default="geom",
                                     help="Bounding box mode")
        self.OptionParser.add_option("--coord_system",
                                     action="store", type="string",
                                     dest="coord_system",
                                     default="desktop",
                                     help="Coordinate system for input")
        self.OptionParser.add_option("--output",
                                     action="store", type="string",
                                     dest="output",
                                     default="set",
                                     help="Output operation")
        self.OptionParser.add_option("--coord_labels",
                                     action="store", type="inkbool",
                                     dest="coord_labels",
                                     default=True,
                                     help="Display coords on-canvas")
        self.OptionParser.add_option("--coord_labels_unit",
                                     action="store", type="string",
                                     dest="coord_labels_unit",
                                     default="uu",
                                     help="Unit for coords on-canvas")
        # notebooks
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     # default='"options_tab"',
                                     help="The selected transform tab")

    # ----- query object order (selection, stack)

    def get_object_by_id(self, node_id):
        """Return SVG element with node_id."""
        if node_id in self.selected:
            return self.selected[node_id]
        else:
            return self.getElementById(node_id)

    # ----- selected bboxes

    def get_bboxes(self, id_list):
        """Return dict with visual or geom bboxes of selected objects."""
        bboxes = {}
        if self.options.bbox_mode == 'visual':
            # visual bbox in root coordinates
            scale = self.unittouu('1px')
            offset = self.get_page_offset()
            bboxes = query_all(self.svg_file, id_list, scale, offset)
        elif self.options.bbox_mode == 'geom':
            # geometric bbox in root coordinates
            for node_id in id_list:
                bbox = geom_bbox(self.get_object_by_id(node_id))
                if bbox is not None:
                    bboxes[node_id] = bbox
        return bboxes if bboxes else None

    # ----- bounding box values

    def get_bbox(self, node_id):
        """Return bbox position and size of node_id."""
        if node_id not in self.bboxes:
            node = self.get_object_by_id(node_id)
            if self.options.bbox_mode == 'geom':
                bbox = geom_bbox(node)
            else:
                bbox = self.get_bboxes([node_id])
            if bbox is not None:
                self.bboxes[node_id] = bbox
        try:
            return self.bboxes[node_id]
        except KeyError:
            inkex.debug('Failed to retrieve bbox for {}'.format(node_id))

    def get_bbox_center(self, node_id):
        """Return center point of bbox rect of node."""
        bbox = self.get_bbox(node_id)
        if bbox is not None:
            x, y, width, height = self.get_bbox(node_id)
            return [x + width/2.0, y + height/2.0]

    # ----- bounding box extents, bounds, ref coords

    def get_bbox_extents(self, node_id):
        """Return extents for bbox rect of node."""
        bbox = self.get_bbox(node_id)
        if bbox is not None:
            return bbox_rect_to_extents(bbox)

    def get_bbox_bounds(self, node_id, orientation=0):
        """Return extents for bbox rect of node."""
        extents = self.get_bbox_extents(node_id)
        if extents is not None:
            if orientation in range(2):  # FIXME: enums in python?
                return extents[orientation*2:orientation*2+2]

    def get_bbox_ref(self, node_id, orientation=0, ref=0):
        """Return ref coordinate for node_id bbox based on orientation."""
        time = 0.0
        refpoints = {'min': 0.0, 'mid': 0.5, 'max': 1.0}
        if ref in refpoints:
            time = refpoints[ref]
        elif isinstance(ref, (int, float)):
            time = float(ref)
        bounds = self.get_bbox_bounds(node_id, orientation)
        if bounds is not None:
            return interpolate(time, bounds[0], bounds[-1])

    def get_bbox_ref_coords(self, node_id, orientation=0, ref=0):
        """Return point or coordinate for bbox ref."""
        if orientation is None:
            return [self.get_bbox_ref(node_id, orientation, ref)
                    for orientation in range(2)]  # FIXME: enums in python?
        else:
            return self.get_bbox_ref(node_id, orientation, ref)

    def get_bbox_min(self, node_id, orientation=None):
        """Return min corner or coordinate of bbox."""
        ref = 0
        return self.get_bbox_ref_coords(node_id, orientation, ref)

    def get_bbox_mid(self, node_id, orientation=None):
        """Return mid point or coordinate of bbox."""
        ref = 0.5
        return self.get_bbox_ref_coords(node_id, orientation, ref)

    def get_bbox_max(self, node_id, orientation=None):
        """Return max corner or coordinate of bbox."""
        ref = 1
        return self.get_bbox_ref_coords(node_id, orientation, ref)

    # ----- draw bbox rect, center markers, center coords

    def draw_rect(self, bbox, color="blue", opacity=0.1):
        """Draw a rect visualizing the bounding box."""
        node = draw_bbox_rect(bbox)
        sdict = simplestyle.parseStyle(node.get('style'))
        sdict['fill'] = color
        sdict['fill-opacity'] = opacity
        node.set('style', simplestyle.formatStyle(sdict))
        self.document.getroot().append(node)

    def draw_cross(self, node_id, center, color="black"):
        """Draw a small cross to mark a transformation center."""
        node = inkex.etree.Element(inkex.addNS('path', 'svg'))
        node.set('id', 'TransformCenter_' + node_id)
        add_cross(node, center, scale=self.unittouu('1px'))
        sdict = simplestyle.parseStyle(node.get('style'))
        sdict['stroke'] = color
        node.set('style', simplestyle.formatStyle(sdict))
        self.document.getroot().append(node)

    def draw_x(self, node_id, center, color="black"):
        """Draw a small x to mark a transformation center."""
        node = inkex.etree.Element(inkex.addNS('path', 'svg'))
        node.set('id', 'TransformCenter_' + node_id)
        add_x(node, center, scale=self.unittouu('1px'))
        sdict = simplestyle.parseStyle(node.get('style'))
        sdict['stroke'] = color
        node.set('style', simplestyle.formatStyle(sdict))
        self.document.getroot().append(node)

    def point_to_options(self, point):
        """Convert point coordinates for canvas text label."""
        # options
        coord_sys = self.options.coord_system
        label_unit = self.options.coord_labels_unit
        # copy point coordinates for label text
        label = point[:]
        # adjust for viewport offset
        offset = self.get_page_offset() or [0.0, 0.0]
        for i in range(2):
            label[i] -= offset[i]
        # convert to coord system
        if coord_sys == 'desktop':
            label[1] = self.get_page_height() - label[1]
        # convert to unit
        if label_unit in ['percent', 'uu']:
            label_unit = ''
        elif label_unit == 'display':
            label_unit = self.getNamedView().get(
                inkex.addNS('document-units', 'inkscape'), 'px')
        if label_unit:
            for i, coord in enumerate(label):
                label[i] = self.uutounit(coord, label_unit)
        return (label, label_unit)

    def format_point(self, point, precision=3):
        """Return text string with formatted coordinates."""
        label, label_unit = self.point_to_options(point)
        text = '('
        text += formatted(label[0], precision)
        text += label_unit
        text += ','
        text += formatted(label[1], precision)
        text += label_unit
        text += ')'
        return text

    def draw_coords(self, points):
        """Draw text with coords of points."""
        # base size for text metrics
        base_px = 10.0
        base_uu = self.unittouu("{}px".format(base_px))
        # text style
        sdict = {}
        # TODO: add style and layout options to INX dialog
        sdict['fill'] = "blue"
        sdict['font-family'] = "monospace"
        sdict['font-size'] = base_uu
        sdict['line-height'] = "1.0"
        sdict['text-anchor'] = "start"
        sdict['text-align'] = "start"
        precision = 3
        # create text objects
        for point in points:
            ctext = inkex.etree.Element(inkex.addNS('text', 'svg'))
            ctext.set('x', str(point[0]))
            ctext.set('y', str(point[1]))
            ctext.set('style', simplestyle.formatStyle(sdict))
            ctext.set(inkex.addNS('space', 'xml'), 'preserve')
            tspan = inkex.etree.Element(inkex.addNS('tspan', 'svg'))
            tspan.text = ' ' + self.format_point(point, precision)
            tspan.set(inkex.addNS('role', 'sodipodi'), 'line')
            ctext.append(tspan)
            self.document.getroot().append(ctext)

    # ----- visualize bbox

    def create_bbox(self, node_id, node):
        """Visualize bounding box with rect or path."""
        bbox = None
        mat = ident_mat()
        # get bbox rect in SVG root coordinates
        root_bbox = self.bboxes.get(node_id)
        if root_bbox is not None and len(root_bbox) == 4:
            # create bbox object
            if self.options.bbox_type == 'rect':
                bbox = draw_bbox_rect(root_bbox)
            elif self.options.bbox_type == 'polyline':
                bbox = draw_bbox_poly(root_bbox, closed=False)
            elif self.options.bbox_type == 'polygon':
                bbox = draw_bbox_poly(root_bbox, closed=True)
            else:
                bbox = draw_bbox_path(root_bbox)
        if bbox is not None:
            # get transform from root to current node
            mat = rootToNodeTransform(node)
            transform = node.get('transform', None)
            if transform is not None:
                node_mat = simpletransform.parseTransform(transform)
                mat = simpletransform.composeTransform(node_mat, mat)
        return bbox, mat

    def draw_bbox(self, node_id):
        """Replace or underlay selected objects with bounding box."""
        node = self.get_object_by_id(node_id)
        bbox, mat = self.create_bbox(node_id, node)
        if bbox is not None:
            parent = node.getparent()
            pos = parent.index(node)
            parent.insert(pos, bbox)
            # transform bbox object from root to node coordinates
            simpletransform.applyTransformToNode(mat, bbox)
            if self.options.copy_style:
                node_style = node.get('style')
                if node_style is not None:
                    bbox.set('style', node_style)
            if self.options.replace:
                bbox.set('id', node.get('id'))
                parent.remove(node)

    # ----- transformation center, angles, lengths

    def length_from_options(self, scope='coord_x'):
        """Return length in user units based on options."""

        def opt_from_scope(name=None):
            """Get option with name *name* from *scope*."""
            sname = scope + ('_{}'.format(name) if name is not None else '')
            return getattr(self.options, sname)

        length = opt_from_scope()
        unit = opt_from_scope('unit')
        if unit == 'percent':
            length /= 100.0
        elif unit == 'uu':
            pass
        elif unit == 'display':
            length = self.convert_from_display_units(length)
        else:
            length = self.unittouu(str(length) + unit)
        return length

    def length_to_options(self, length, scope='coord_x'):
        """Return uu length in custom units based on options."""

        def opt_from_scope(name=None):
            """Get option with name *name* from *scope*."""
            sname = scope + ('_{}'.format(name) if name is not None else '')
            return getattr(self.options, sname)

        unit = opt_from_scope('unit')
        if unit == 'percent':
            length *= 100.0
        elif unit == 'uu':
            pass
        elif unit == 'display':
            length = self.convert_to_display_units(length)
        else:
            length = self.uutounit(length, unit)
        return length

    def center_to_options(self, node_id, center):
        """Get coords for center based on user input options."""
        # pylint: disable=too-many-branches
        origin = [0.0, 0.0]
        offset = self.get_page_offset() or [0.0, 0.0]
        coord_sys = self.options.coord_system
        coord_ref = [self.options.coord_x_object_ref,
                     self.options.coord_y_object_ref]
        coord_unit = [self.options.coord_x_unit,
                      self.options.coord_y_unit]
        # copy position of input in absolute coords (uu, root, svg)
        coords = list(center)
        # convert to coordinates relative to refpoint
        for i in range(2):
            if coord_ref[i] == 'reset':
                coords[i] = self.get_bbox_ref_coords(node_id, i, 0.5)
            elif coord_ref[i] == 'bbox':
                if coord_unit[i] == 'percent':
                    bounds = self.get_bbox_bounds(node_id, i)
                    coords[i] = parameterize(coords[i], bounds[0], bounds[-1])
                else:
                    if i == 1 and coord_sys == 'desktop':
                        coords[i] -= self.get_bbox_ref_coords(node_id, i, 1)
                    else:
                        coords[i] -= self.get_bbox_ref_coords(node_id, i, 0)
            elif coord_ref[i] == 'page':
                if coord_unit[i] == 'percent':
                    bounds = (offset[i] + origin[i],
                              offset[i] + self.get_page_size()[i])
                    coords[i] = parameterize(coords[i], bounds[0], bounds[-1])
                else:
                    coords[i] -= offset[i] + origin[i]
            elif coord_ref[i] == 'current':
                if coord_unit[i] == 'percent':
                    bounds = (self.get_bbox_ref(node_id, i, 0.5), center[i])
                    coords[i] = parameterize(coords[i], bounds[0], bounds[-1])
                    if i == 1 and coord_sys == 'desktop':
                        coords[i] = 1 - coords[i]  # FIXME: undo later flip
                else:
                    coords[i] -= center[i]
        # convert to desktop coordinates
        if coord_sys == 'desktop':
            if coord_unit[1] == 'percent':
                coords[1] = 1 - coords[1]
            elif coord_ref[1] == 'origin':
                coords[1] = self.get_page_height() - coords[1]
            elif coord_ref[1] == 'current':
                coords[1] *= -1
            else:  # vertical distance to refpoint
                coords[1] *= -1
        # convert to selected units
        coords = [self.length_to_options(length, scope)
                  for length, scope in zip(coords, ['coord_x', 'coord_y'])]
        # return option values for current transformation center
        return coords

    def center_from_options(self, node_id, center):
        """Return new position of transformation center."""
        # pylint: disable=too-many-branches
        origin = [0.0, 0.0]
        offset = self.get_page_offset() or [0.0, 0.0]
        coord_sys = self.options.coord_system
        coord_ref = [self.options.coord_x_object_ref,
                     self.options.coord_y_object_ref]
        coord_unit = [self.options.coord_x_unit,
                      self.options.coord_y_unit]
        # values for new transformation center converted to user units
        coords = [self.length_from_options(scope)
                  for scope in ['coord_x', 'coord_y']]
        # convert from desktop coordinates
        if coord_sys == 'desktop':
            if coord_unit[1] == 'percent':
                coords[1] = 1 - coords[1]
            elif coord_ref[1] == 'page':
                coords[1] = self.get_page_height() - coords[1]
            elif coord_ref[1] == 'current':
                coords[1] *= -1
            else:
                coords[1] *= -1
        # convert refpoint distance to absolute coords in SVGRoot
        for i in range(2):
            if coord_ref[i] == 'reset':
                coords[i] = None
            elif coord_ref[i] == 'bbox':
                if coord_unit[i] == 'percent':
                    bounds = self.get_bbox_bounds(node_id, i)
                    coords[i] = interpolate(coords[i], bounds[0], bounds[-1])
                else:
                    if i == 1 and coord_sys == 'desktop':
                        coords[i] += self.get_bbox_ref_coords(node_id, i, 1)
                    else:
                        coords[i] += self.get_bbox_ref_coords(node_id, i, 0)
            elif coord_ref[i] == 'page':
                if coord_unit[i] == 'percent':
                    bounds = (offset[i] + origin[i],
                              offset[i] + self.get_page_size()[i])
                    coords[i] = interpolate(coords[i], bounds[0], bounds[-1])
                else:
                    coords[i] += offset[i] + origin[i]
            elif coord_ref[i] == 'current':
                if coord_unit[i] == 'percent':
                    # interpolate distance between bbox midpoint and center
                    if i == 1 and coord_sys == 'desktop':
                        coords[i] = 1 - coords[i]  # FIXME: undo earlier flip
                    bounds = (self.get_bbox_ref(node_id, i, 0.5), center[i])
                    coords[i] = interpolate(coords[i], bounds[0], bounds[-1])
                else:
                    coords[i] += center[i]
        # return new coordinates in user units, SVGRoot coords
        return coords

    # ----- transformation center

    def set_transform_center(self, node_id):
        """Set new position of transformation center."""
        ret = []
        # bbox midpoint
        midpoint = self.get_bbox_mid(node_id)
        if midpoint is not None:
            # absolute coords of current transformation center
            node = self.get_object_by_id(node_id)
            old = get_inkscape_transform_center(node, midpoint)
            ret.append(old)
            if old is not None:
                # absolute coords for new transformation center
                new = self.center_from_options(node_id, old)
                if new is not None:
                    result = set_inkscape_transform_center(node, midpoint, new)
                    ret.append(result)
        return ret

    def get_transform_center(self, node_id):
        """Get current position of transformation center."""
        ret = []
        # bbox midpoint
        midpoint = self.get_bbox_mid(node_id)
        if midpoint is not None:
            # absolute coords of current transformation center
            node = self.get_object_by_id(node_id)
            old = get_inkscape_transform_center(node, midpoint)
            ret.append(old)
            if old is not None:
                # absolute coords for new transformation center
                new = self.center_from_options(node_id, old)
                if new is not None:
                    # check new
                    for i in range(2):
                        if new[i] is None:
                            # unset value: fall back to bbox midpoint
                            new[i] = midpoint[i]
                    ret.append(new)
        return ret

    def draw_transform_center(self, node_id):
        """Draw current and new position of transformation center."""
        # bbox midpoint
        midpoint = self.get_bbox_mid(node_id)
        if midpoint is not None:
            # draw bbox midpoint
            self.draw_x("main", midpoint, color="black")
            if self.options.coord_labels:
                self.draw_coords([midpoint])
            # current transformation center
            node = self.get_object_by_id(node_id)
            old = get_inkscape_transform_center(node, midpoint)
            if old is not None:
                # draw old
                self.draw_x("old", old, color="blue")
                if self.options.coord_labels:
                    self.draw_coords([old])
                # new transformation center
                new = self.center_from_options(node_id, old)
                if new is not None:
                    # check new
                    for i in range(2):
                        if new[i] is None:
                            # unset value: fall back to bbox midpoint
                            new[i] = midpoint[i]
                    # draw new
                    self.draw_cross("new", new, color="red")
                    if self.options.coord_labels:
                        self.draw_coords([new])

    # ----- main

    def transform_center(self, node_id):
        """Set or get Inkscape transformation center from node_id."""
        if self.options.output == 'object':
            # set new position of transformation center
            result = self.set_transform_center(node_id)
            if DEBUG:
                for point in result:
                    inkex.debug(self.format_point(point, precision=3))
        elif self.options.output == 'stderr':
            # return old and new position via stderr
            result = self.get_transform_center(node_id)
            for point in result:
                inkex.debug(self.format_point(point, precision=3))
        elif self.options.output == 'canvas':
            # draw markers for bbox midpoint, old and new center
            # annotate each marker with coord values
            self.draw_transform_center(node_id)

    # ----- entry

    def check_objects(self, id_list):
        """Check for unsupported element types."""
        skipped = []
        for node_id in id_list:
            node = self.get_object_by_id(node_id)
            # geom bbox method has no support for text objects
            if is_text(node):
                skipped.append(node_id)
        if len(skipped):
            for node_id in skipped:
                if node_id in id_list:
                    id_list.remove(node_id)
            inkex.debug("Unsupported elements have been skipped.")

    def effect(self):
        """Main entry point to process current document."""
        # get z-sorted list of selected objects
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        # override bbox mode in case of previously selected visual mode
        self.options.bbox_mode = 'geom'
        # check objects (not all are supported by computeBBox())
        self.check_objects(id_list)
        # cache dict with visual or geometrix bbox values
        self.bboxes = self.get_bboxes(id_list)
        # actions
        if self.bboxes is not None and len(self.bboxes) == len(id_list):
            if self.options.action == 'draw_bbox':
                for node_id in reversed(id_list):
                    self.draw_bbox(node_id)
            elif self.options.action == 'transformation_center':
                for node_id in reversed(id_list):
                    self.transform_center(node_id)
        else:
            # TODO: provide more information
            inkex.debug("Selection contains unsupported elements.")


if __name__ == '__main__':
    ME = TransformCenter()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
