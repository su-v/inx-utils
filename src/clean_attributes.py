#!/usr/bin/env python
"""
clean_attributes - remove attributes in custom namespaces

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors

# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex


__version__ = '0.0'


# ----- SVG helper functions

def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


def namespace_kw(namespace):
    """Find namespace shortref in inkex.NSS."""
    for key, val in inkex.NSS.items():
        if val == namespace:
            return key


def list_attribs(node_id, node):
    """List all attributes of object *node*."""
    inkex.debug('===== Attributes of object: {} ====='.format(node_id))
    for attr_name in sorted(node.attrib):
        attr_val = node.attrib[attr_name]
        q_name = inkex.etree.QName(attr_name)
        if q_name.namespace:
            kw_nss = namespace_kw(q_name.namespace)
            shortname = '{}:{}'.format(kw_nss, q_name.localname)
        else:
            shortname = q_name.localname
        if len(attr_val) > 72:
            shortval = attr_val[:56] + '...'
        else:
            shortval = attr_val
        inkex.debug('{} = {}'.format(shortname, shortval))
    inkex.debug('')


def remove_attribs(node, namespaces):
    """Remove object attributes in custom namespaces."""
    for attr_name in sorted(node.attrib.keys()):
        q_name = inkex.etree.QName(attr_name)
        for namespace in namespaces:
            if inkex.NSS[namespace] == q_name.namespace:
                node.attrib.pop(attr_name, None)


# ----- main class

class CleanAttributes(inkex.Effect):
    """Effect-based class to remove attributes in custom namespaces."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # options
        self.OptionParser.add_option("--list_all",
                                     action="store", type="inkbool",
                                     dest="list_all", default=True,
                                     help="List all attributes of object")
        self.OptionParser.add_option("--remove_inkscape_ns",
                                     action="store", type="inkbool",
                                     dest="remove_inkscape_ns", default=False,
                                     help="Remove attr in inkscape namespace")
        self.OptionParser.add_option("--remove_sodipodi_ns",
                                     action="store", type="inkbool",
                                     dest="remove_sodipodi_ns", default=False,
                                     help="Remove attr in sodipodi namespace")

    def effect(self):
        """Main entry point to process current document."""
        # get sorted list of ids
        root = self.document.getroot()
        id_list = z_iter(root, self.selected.keys())
        # options
        namespaces = []
        if self.options.remove_inkscape_ns:
            namespaces.append('inkscape')
        if self.options.remove_sodipodi_ns:
            namespaces.append('sodipodi')
        # action
        for node_id in id_list:
            node = self.getElementById(node_id)
            if self.options.list_all:
                list_attribs(node_id, node)
            if len(namespaces):
                remove_attribs(node, namespaces)


if __name__ == '__main__':
    ME = CleanAttributes()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
