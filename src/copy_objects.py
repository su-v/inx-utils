#!/usr/bin/env python
"""
copy_objects - copy or clone multiple objects N times

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors

# standard libraries
import copy

# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex


__version__ = '0.0'


# ----- SVG helper functions

def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


# ----- main class

class CopyObjects(inkex.Effect):
    """Effect-based class to copy objects multiple times."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # options
        self.OptionParser.add_option("--action",
                                     action="store", type="string",
                                     dest="action", default="copy",
                                     help="Selected action")
        self.OptionParser.add_option("--count",
                                     action="store", type="int",
                                     dest="count", default=9,
                                     help="Number of copies")

    def clone(self, node_id, count):
        """Clone object with node_id *count* times."""
        node = self.getElementById(node_id)
        if node is not None:
            parent = node.getparent()
            index = parent.index(node) + 1
            href = '#{}'.format(node_id)
            for _ in range(count):
                new_id = self.uniqueId('use')
                new_node = inkex.etree.Element(inkex.addNS('use', 'svg'))
                new_node.set(inkex.addNS('href', 'xlink'), href)
                new_node.set('id', new_id)
                parent.insert(index, new_node)

    def copy(self, node_id, count):
        """Copy object with node_id *count* times."""
        node = self.getElementById(node_id)
        if node is not None:
            parent = node.getparent()
            index = parent.index(node) + 1
            for _ in range(count):
                new_id = self.uniqueId(node_id)
                new_node = copy.deepcopy(node)
                new_node.set('id', new_id)
                parent.insert(index, new_node)

    def effect(self):
        """Main entry point to process current document."""
        # get sorted list of ids
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        if self.options.action == 'copy':
            for node_id in id_list:
                self.copy(node_id, self.options.count)
        elif self.options.action == 'clone':
            for node_id in id_list:
                self.clone(node_id, self.options.count)


if __name__ == '__main__':
    ME = CopyObjects()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
