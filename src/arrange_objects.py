#!/usr/bin/env python
"""
arrange_objects - arrange object based on selection or object bbox

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors
# pylint: disable=too-many-lines

# inkscape library
try:
    import inkex_local as inkex
except ImportError:
    import inkex
import simplestyle

# local library
import transform_objects as tfo
from transform_objects import z_sort, tab_name, TransformBasic


__version__ = '0.0'


DEBUG = False
AXIS_X_KEY = 'x'
AXIS_Y_KEY = 'y'


# ----- utility functions

def strip_end(text, suffix):
    """Strip suffix from string *text*."""
    if not text.endswith(suffix):
        return text
    return text[:len(text)-len(suffix)]


def is_metadata(node):
    """Check whether node is of type SVG metadata."""
    return node.tag == inkex.addNS('metadata', 'svg')


def is_style(node):
    """Check whether node is of type SVG style."""
    return node.tag == inkex.addNS('style', 'svg')


def is_script(node):
    """Check whether node is of type SVG script."""
    return node.tag == inkex.addNS('script', 'svg')


def is_defs(node):
    """Check whether node is of type SVG defs."""
    return node.tag == inkex.addNS('defs', 'svg')


def is_group(node):
    """Check whether node is of type SVG group."""
    return node.tag == inkex.addNS('g', 'svg')


def is_layer(node):
    """Check node for group and groupmode 'layer'."""
    return (is_group(node) and
            node.get(inkex.addNS('groupmode', 'inkscape')) == 'layer')


def is_namedview(node):
    """Check whether node is of type Sodipodi namedview."""
    return node.tag == inkex.addNS('namedview', 'sodipodi')


# ----- directional params

def get_params_default():
    """Return default params dict for action methods."""
    return {AXIS_X_KEY: dict(),
            AXIS_Y_KEY: dict()}


def get_axis_index(key):
    """Return index for coordinate axis based on axis key."""
    return [AXIS_X_KEY,
            AXIS_Y_KEY].index(key)


def get_highlight_color(key):
    """"Return default color based on axis."""
    colors = {AXIS_X_KEY: "red",
              AXIS_Y_KEY: "blue"}
    return colors.get(key, "red")


def get_params_objects_set(params):
    """Return a set of object ids from records of params dict."""
    objects = set()
    for axis in params.keys():
        objects.update(params[axis].keys())
    return objects


# ----- transform helper functions

def minmax_factors(ref):
    """Return factors for minmax bounds based on *ref*."""
    if ref == 'max':
        return (0.0, 1.0)
    elif ref == 'mid':
        return (0.5, 0.5)
    else:  # if ref == 'min':
        return (1.0, 0.0)


def ref_coord(ref, bounds):
    """Return refpoint coordinate from bounds."""
    fmin, fmax = minmax_factors(ref)
    return fmin*bounds[0] + fmax*bounds[1]


# ----- transform helper functions: stretch

def stretch_factor(check, length, new_length, anchor):
    """Return stretch factor and anchor based on check."""
    ratio = 1.0
    if check:
        try:
            ratio = new_length / length
        except ZeroDivisionError:
            anchor = None
    else:
        anchor = None
    return ratio, anchor


def get_stretch_object_params(tdict, object_bounds):
    """Return object stretch params based on anchor ref, object bounds."""
    # pylint: disable=too-many-branches
    a_min, a_max = tdict['anchor_bounds']       # anchor area extents
    a_mid = 0.5*a_min + 0.5*a_max               # anchor area midpoint
    o_min, o_max = object_bounds                # object bbox bounds
    fmin, fmax = minmax_factors(tdict['object_ref'])
    o_ref = fmin*o_min + fmax*o_max             # object refpoint coord
    check = False
    old = o_max - o_min
    new = old
    ref = None
    edge = tdict['anchor_ref']
    if edge == 'min':
        if tfo.isclose(a_min - o_ref, 0, abs_tol=1e-05):
            pass                            # object refpoint on edge
        elif o_ref < a_min:                 # object refpoint outside
            check = True  # 'outside'
            new = a_min - o_min
            ref = o_min
        elif o_ref > a_min:                 # object refpoint inside
            check = True  # 'inside'
            new = o_max - a_min
            ref = o_max
    elif edge == 'mid':
        if tfo.isclose(a_mid - o_ref, 0, abs_tol=1e-05):
            pass                            # object refpoint on edge
        elif o_ref < a_mid:                 # object refpoint smaller
            check = True
            new = a_mid - o_min
            ref = o_min
        elif o_ref > a_mid:                 # object refpoint larger
            check = True
            new = o_max - a_mid
            ref = o_max
    elif edge == 'max':
        if tfo.isclose(a_max - o_ref, 0, abs_tol=1e-05):
            pass                            # object refpoint on edge
        elif o_ref > a_max:                 # object refpoint outside
            check = True  # 'outside'
            new = o_max - a_max
            ref = o_max
        elif o_ref < a_max:                 # object refpoint inside
            check = True  # 'inside'
            new = a_max - o_min
            ref = o_min
    return stretch_factor(check, old, new, ref)


class TransformObjects(TransformBasic):
    """Effect-based class to transform selected objects."""
    # pylint: disable=too-many-public-methods

    def __init__(self):
        """Init base class."""
        TransformBasic.__init__(self)

        # presets
        self.OptionParser.add_option("--preset_objects",
                                     action="store", type="string",
                                     dest="preset_objects",
                                     default="selection",
                                     help="Objects for preset")
        self.OptionParser.add_option("--preset_action",
                                     action="store", type="string",
                                     dest="preset_action",
                                     default="align_page_mid",
                                     help="Preset action")
        # align objects
        self.OptionParser.add_option("--align_objects",
                                     action="store", type="string",
                                     dest="align_objects",
                                     default="selection",
                                     help="Align objects")
        # align options
        self.OptionParser.add_option("--align_x_anchor",
                                     action="store", type="string",
                                     dest="align_x_anchor",
                                     default="none",
                                     help="Align x: anchor")
        self.OptionParser.add_option("--align_x_anchor_ref",
                                     action="store", type="string",
                                     dest="align_x_anchor_ref",
                                     default="min",
                                     help="Align x: anchor refpoint")
        self.OptionParser.add_option("--align_x_object_ref",
                                     action="store", type="string",
                                     dest="align_x_object_ref",
                                     default="min",
                                     help="Align x: object refpoint")
        self.OptionParser.add_option("--align_y_anchor",
                                     action="store", type="string",
                                     dest="align_y_anchor",
                                     default="none",
                                     help="Align y: anchor")
        self.OptionParser.add_option("--align_y_anchor_ref",
                                     action="store", type="string",
                                     dest="align_y_anchor_ref",
                                     default="min",
                                     help="Align y: anchor refpoint")
        self.OptionParser.add_option("--align_y_object_ref",
                                     action="store", type="string",
                                     dest="align_y_object_ref",
                                     default="min",
                                     help="Align y: object refpoint")
        self.OptionParser.add_option("--align_x_context",
                                     action="store", type="string",
                                     dest="align_x_context",
                                     default="individual",
                                     help="Align x: context")
        self.OptionParser.add_option("--align_y_context",
                                     action="store", type="string",
                                     dest="align_y_context",
                                     default="individual",
                                     help="Align y: context")
        # distribute objects
        self.OptionParser.add_option("--distribute_objects",
                                     action="store", type="string",
                                     dest="distribute_objects",
                                     default="selection",
                                     help="Distribute objects")
        # distribute options
        self.OptionParser.add_option("--distribute_x_anchor",
                                     action="store", type="string",
                                     dest="distribute_x_anchor",
                                     default="none",
                                     help="Distribute x: anchor")
        self.OptionParser.add_option("--distribute_x_anchor_ref",
                                     action="store", type="string",
                                     dest="distribute_x_anchor_ref",
                                     default="extents",
                                     help="Distribute x: anchor refpoint")
        self.OptionParser.add_option("--distribute_x_object_ref",
                                     action="store", type="string",
                                     dest="distribute_x_object_ref",
                                     default="min",
                                     help="Distribute x: object refpoint")
        self.OptionParser.add_option("--distribute_y_anchor",
                                     action="store", type="string",
                                     dest="distribute_y_anchor",
                                     default="none",
                                     help="Distribute y: anchor")
        self.OptionParser.add_option("--distribute_y_anchor_ref",
                                     action="store", type="string",
                                     dest="distribute_y_anchor_ref",
                                     default="extents",
                                     help="Distribute y: anchor refpoint")
        self.OptionParser.add_option("--distribute_y_object_ref",
                                     action="store", type="string",
                                     dest="distribute_y_object_ref",
                                     default="min",
                                     help="Distribute y: object refpoint")
        # stretch objects
        self.OptionParser.add_option("--stretch_objects",
                                     action="store", type="string",
                                     dest="stretch_objects",
                                     default="selection",
                                     help="Stretch objects")
        # stretch options
        self.OptionParser.add_option("--stretch_x_anchor",
                                     action="store", type="string",
                                     dest="stretch_x_anchor",
                                     default="none",
                                     help="Stretch x: anchor")
        self.OptionParser.add_option("--stretch_x_anchor_ref",
                                     action="store", type="string",
                                     dest="stretch_x_anchor_ref",
                                     default="min",
                                     help="Stretch x: anchor refpoint")
        self.OptionParser.add_option("--stretch_x_object_ref",
                                     action="store", type="string",
                                     dest="stretch_x_object_ref",
                                     default="mid",
                                     help="Stretch x: object refpoint")
        self.OptionParser.add_option("--stretch_y_anchor",
                                     action="store", type="string",
                                     dest="stretch_y_anchor",
                                     default="none",
                                     help="Stretch y: anchor")
        self.OptionParser.add_option("--stretch_y_anchor_ref",
                                     action="store", type="string",
                                     dest="stretch_y_anchor_ref",
                                     default="min",
                                     help="Stretch y: anchor refpoint")
        self.OptionParser.add_option("--stretch_y_object_ref",
                                     action="store", type="string",
                                     dest="stretch_y_object_ref",
                                     default="mid",
                                     help="Stretch y: object refpoint")
        # re-arrange objects
        self.OptionParser.add_option("--rearrange_objects",
                                     action="store", type="string",
                                     dest="rearrange_objects",
                                     default="selection",
                                     help="Rearrange objects")
        # re-arrange options
        # overlaps objects
        self.OptionParser.add_option("--overlaps_objects",
                                     action="store", type="string",
                                     dest="overlaps_objects",
                                     default="selection",
                                     help="Overlaps objects")
        # overlaps options
        # common options
        self.OptionParser.add_option("--highlight_anchor",
                                     action="store", type="inkbool",
                                     dest="highlight_anchor", default=False,
                                     help="Highlight anchor")
        # notebooks
        self.OptionParser.add_option("--transform_notebook",
                                     action="store", type="string",
                                     dest="transform_notebook",
                                     default='"align_tab"',
                                     help="The selected transform tab")
        # dispatcher
        self.OptionParser.add_option("--extension",
                                     action="store", type="string",
                                     dest="extension",
                                     default="transform_objects",
                                     help="[hidden] extension parameter")

    # ----- query object order (selection, stack)

    def get_object_by_id(self, node_id):
        """Return SVG element with node_id."""
        if node_id in self.selected:
            return self.selected[node_id]
        else:
            return self.getElementById(node_id)

    def get_first_selected(self, id_list):
        """Return id of first selected object in id_list."""
        for node_id in self.options.ids:
            if node_id in id_list:
                return node_id

    def get_last_selected(self, id_list):
        """Return id of last selected object in id_list."""
        for node_id in reversed(self.options.ids):
            if node_id in id_list:
                return node_id

    def get_smallest_object(self, id_list, axis):
        """Return id of smallest object in id_list."""
        self.update_bboxes(id_list)
        sorted_ids = self.get_sorted_ids(sortby='size')[axis]
        for node_id in sorted_ids:
            if node_id in id_list:
                # skip objects with zero dimension (text anchor in geom bbox)
                if self.get_bbox_size(node_id, axis):
                    return node_id

    def get_biggest_object(self, id_list, axis):
        """Return id of biggest object in id_list."""
        self.update_bboxes(id_list)
        sorted_ids = self.get_sorted_ids(sortby='size')[axis]
        for node_id in reversed(sorted_ids):
            if node_id in id_list:
                # skip objects with zero dimension (text anchor in geom bbox)
                if self.get_bbox_size(node_id, axis):
                    return node_id

    def get_bottom_object(self, id_list):
        """Return id of bottom-most object in id_list."""
        root = self.document.getroot()
        return z_sort(root, id_list)[0]

    def get_top_object(self, id_list):
        """Return id of top-most object in id_list."""
        root = self.document.getroot()
        return z_sort(root, id_list)[-1]

    # ----- drawing area

    def get_drawing_extents(self):
        """Return extents of drawing area."""
        if self.drawing_area is None:
            self.get_bbox_values(None)
        return tfo.bbox_rect_to_extents(self.drawing_area)

    # ----- query bboxes

    def get_bbox_values(self, node_id=None):
        """Return bbox values from cache or compute geom bbox."""
        fallback = 4 * [0.0]
        root = self.document.getroot()
        root_id = root.get('id')
        if self.options.bbox_mode == 'visual':
            if self.bboxes is None:
                scale = self.unittouu('1px')
                offset = self.get_page_offset()
                # cache values in instance attributes
                self.bboxes = tfo.query_all(self.svg_file, None, scale, offset)
                self.drawing_area = self.bboxes.pop(root_id)
            # check bboxes for node_id
            if node_id is None or node_id == root_id:
                return self.drawing_area
        else:
            # init bboxes dictionary
            if self.bboxes is None:
                self.bboxes = dict()
            # drawing area
            if self.drawing_area is None:
                bbox = tfo.get_geom_bbox(root)
                if bbox is None:
                    # if only text was selected, resort to query_all()
                    scale = self.unittouu('1px')
                    offset = self.get_page_offset()
                    temp = tfo.query_all(self.svg_file, [root_id], scale,
                                         offset)
                    bbox = temp.pop(root_id)
                # cache root bbox in instance attribute
                self.drawing_area = bbox
            # check bboxes for node_id
            if node_id is None or node_id == root_id:
                return self.drawing_area
            elif node_id not in self.bboxes:
                node = self.get_object_by_id(node_id)
                bbox = tfo.get_geom_bbox(node)
                if bbox is None and tfo.is_regular_text(node):
                    x, y = tfo.get_text_pos(node, root=True)
                    bbox = [x, y, 0.0, 0.0]
                # cache computed value in instance attribute
                if bbox is not None:
                    self.bboxes[node_id] = bbox
        # return from updated cache
        return self.bboxes.get(node_id, fallback)

    def get_bbox_extents(self, node_id):
        """Return extents for bbox rect of node."""
        bbox = self.get_bbox_values(node_id)
        return tfo.bbox_rect_to_extents(bbox)

    def get_bbox_bounds(self, node_id, axis):
        """Return bbox bounds for axis."""
        pos = get_axis_index(axis)
        extents = self.get_bbox_extents(node_id)
        return extents[pos*2:pos*2+2]

    def get_bbox_size(self, node_id, axis):
        """Return bbox size for axis."""
        pos = get_axis_index(axis)
        bbox = self.get_bbox_values(node_id)
        return bbox[pos+2]

    def get_bbox_center(self, node_id):
        """Return center point of bbox rect of node."""
        x, y, width, height = self.get_bbox_values(node_id)
        return [x + width/2.0, y + height/2.0]

    def get_bbox_min(self, node_id):
        """Return min corner of bbox."""
        xmin, _, ymin, _ = self.get_bbox_extents(node_id)
        return [xmin, ymin]

    def get_bbox_mid(self, node_id):
        """Return mid point of bbox."""
        return self.get_bbox_center(node_id)

    def get_bbox_max(self, node_id):
        """Return max corner of bbox."""
        _, xmax, _, ymax = self.get_bbox_extents(node_id)
        return [xmax, ymax]

    def get_bbox_coord(self, node_id, ref, axis):
        """Return bbox coord based on refpoint, axis."""
        if ref == 'min':
            point = self.get_bbox_min(node_id)
        elif ref == 'mid':
            point = self.get_bbox_mid(node_id)
        elif ref == 'max':
            point = self.get_bbox_max(node_id)
        pos = get_axis_index(axis)
        return point[pos]

    # ----- selection area

    def get_bboxes_extents(self, id_list):
        """Return extents of the combined bboxes of objects in id_list."""
        # pylint: disable=arguments-differ
        extents = self.get_bbox_extents(id_list[0])
        for node_id in id_list[1:]:
            xmin, xmax, ymin, ymax = self.get_bbox_extents(node_id)
            extents[0] = min(xmin, extents[0])
            extents[1] = max(xmax, extents[1])
            extents[2] = min(ymin, extents[2])
            extents[3] = max(ymax, extents[3])
        return extents

    def get_bboxes_bounds(self, id_list, axis):
        """Return bounds of bbox union of id_list for axis."""
        pos = get_axis_index(axis)
        extents = self.get_bboxes_extents(id_list)
        return extents[pos*2:pos*2+2]

    def get_bboxes_size(self, id_list, axis):
        """Return size of bbox union of id_list for axis."""
        bounds = self.get_bboxes_bounds(id_list, axis)
        return bounds[1] - bounds[0]

    def get_bboxes_size_sum(self, id_list, axis):
        """Return sum of all bbox sizes of objects in id_list."""
        return sum(self.get_bbox_size(node_id, axis) for node_id in id_list)

    def update_bboxes(self, id_list):
        """Force caching of bbox values."""
        if self.bboxes is None:
            self.get_bbox_values(None)
        if self.options.bbox_mode == 'geom':
            for node_id in id_list:
                if node_id not in self.bboxes:
                    self.get_bbox_values(node_id)

    # ----- debug methods

    def highlight(self, node_id, color="red"):
        """Color object with node_id using custom fill color."""
        node = self.get_object_by_id(node_id)
        sdict = simplestyle.parseStyle(node.get('style'))
        if 'stroke' in sdict and sdict['stroke'] != 'none':
            sdict['stroke'] = color
        else:
            sdict['fill'] = color
        node.set('style', simplestyle.formatStyle(sdict))

    # ----- transform helper methods

    def flip_vertical_refs(self, action):
        """Flip vertical refpoint labels for desktop coordinates."""
        if self.options.coord_system == 'desktop':
            switch = {'min': 'max', 'max': 'min'}
            for target in ['anchor', 'object']:
                name = '{}_{}_{}_ref'.format(action, AXIS_Y_KEY, target)
                param = getattr(self.options, name)
                if param in switch:
                    setattr(self.options, name, switch[param])

    def check_text_objects(self, object_ref, id_list):
        """Check for supported text objects."""
        # pylint: disable=too-many-branches
        skipped = []
        # booleans for text objects and text anchors
        no_flowed_text = False
        no_text_anchors = False
        no_regular_text = False
        # check object refpoint with bbox mode
        if object_ref.endswith('text'):
            # update object_ref to min ref position
            object_ref = 'min'
            # check bbox mode
            no_flowed_text = bool(self.options.bbox_mode == 'geom')
            no_text_anchors = bool(self.options.bbox_mode == 'visual')
        else:
            no_regular_text = bool(self.options.bbox_mode == 'geom')
        # skip unsupported elements in id_list
        if no_text_anchors:
            # text anchors are not supported in visual bbox mode
            skipped = id_list
            has_skipped_text = False
            for node_id in id_list:
                if tfo.is_text(self.get_object_by_id(node_id)):
                    has_skipped_text = True
                    break
            if has_skipped_text:
                inkex.debug("Text anchors are not supported "
                            "in visual bbox mode.")
        elif no_flowed_text:
            # text anchors in geom bbox mode only support regular text
            has_skipped_flowed_text = False
            for node_id in id_list:
                node = self.get_object_by_id(node_id)
                if tfo.is_flowed_text(node):
                    has_skipped_flowed_text = True
                if not tfo.is_regular_text(node):
                    skipped.append(node_id)
            if has_skipped_flowed_text:
                inkex.debug("Flowed text objects have been skipped.")
        elif no_regular_text:
            # other anchors in geom bbox mode only support flowed text
            has_skipped_regular_text = False
            for node_id in id_list:
                node = self.get_object_by_id(node_id)
                if tfo.is_regular_text(node):
                    has_skipped_regular_text = True
                    skipped.append(node_id)
            if has_skipped_regular_text:
                inkex.debug("Regular text objects have been skipped.")
        return object_ref, skipped

    def get_anchor_bounds(self, axis, anchor, id_list):
        """Return anchor bounds, pinned anchor objects."""
        pinned = []
        extents = 4 * [0.0]  # TODO: useful fallback value for extents?
        if len(id_list):
            if anchor == 'selection':
                extents = self.get_bboxes_extents(id_list)
            elif anchor == 'page':
                extents = self.get_page_extents()
            elif anchor == 'drawing':
                extents = self.get_drawing_extents()
            elif anchor == 'last':
                anchor_id = self.get_last_selected(id_list)
                extents = self.get_bbox_extents(anchor_id)
                pinned.append(anchor_id)
            elif anchor == 'first':
                anchor_id = self.get_first_selected(id_list)
                extents = self.get_bbox_extents(anchor_id)
                pinned.append(anchor_id)
            elif anchor == 'smallest':
                anchor_id = self.get_smallest_object(id_list, axis)
                extents = self.get_bbox_extents(anchor_id)
                pinned.append(anchor_id)
            elif anchor == 'biggest':
                anchor_id = self.get_biggest_object(id_list, axis)
                extents = self.get_bbox_extents(anchor_id)
                pinned.append(anchor_id)
            elif anchor == 'bottom':
                anchor_id = self.get_bottom_object(id_list)
                extents = self.get_bbox_extents(anchor_id)
                pinned.append(anchor_id)
            elif anchor == 'top':
                anchor_id = self.get_top_object(id_list)
                extents = self.get_bbox_extents(anchor_id)
                pinned.append(anchor_id)
        bounds = {AXIS_X_KEY: extents[0:2], AXIS_Y_KEY: extents[2:4]}
        return bounds[axis], pinned

    # ----- transform helper methods: distribute gaps

    def set_distribute_gap_object(self, tdict, axis, sorted_ids, deltas):
        """Compute deltas for gaps filling anchor object bounds."""
        # pylint: disable=too-many-locals
        pinned = None
        # check number of objects
        if len(sorted_ids) < 3:
            msg = "'Distribute' requires 3 or more objects."
            return msg
        # get pinned object
        if len(tdict['exclude']):
            pinned = tdict['exclude'][0]
        if pinned is not None:
            pinned_index = sorted_ids.index(pinned)
            deltas[pinned] = 0.0
            # remove pinned object from sorted ids
            sorted_ids.pop(pinned_index)
            # compute gap relative to area of pinned object
            target_bounds = self.get_bbox_bounds(pinned, axis)
            target_size = target_bounds[1] - target_bounds[0]
            bboxes_size = self.get_bboxes_size_sum(sorted_ids, axis)
            offset = (target_size - bboxes_size) / (len(sorted_ids) - 1)
            # cache max from first object
            prev_max = self.get_bbox_coord(pinned, "min", axis) - offset
            # compute delta for each id in sorted_ids
            for node_id in sorted_ids:
                old_min, old_max = self.get_bbox_bounds(node_id, axis)
                new_min = prev_max + offset
                new_max = old_max + (new_min - old_min)
                # update cache for previous edge
                prev_max = new_max
                # store absolute delta per node_id
                deltas[node_id] = new_min - old_min

    def set_distribute_gap_pinned(self, tdict, axis, sorted_ids, deltas):
        """Compute deltas for gaps from pinned object."""
        # pylint: disable=too-many-locals
        pinned = None
        # check number of objects
        if len(sorted_ids) < 2:
            msg = "'Distribute' requires 2 or more objects."
            return msg
        # get pinned object
        if len(tdict['exclude']):
            pinned = tdict['exclude'][0]
        if pinned is not None:
            pinned_index = sorted_ids.index(pinned)
            deltas[pinned] = 0.0
            # compute gap relative to selection area
            target_size = self.get_bboxes_size(sorted_ids, axis)
            bboxes_size = self.get_bboxes_size_sum(sorted_ids, axis)
            offset = (target_size - bboxes_size) / (len(sorted_ids) - 1)
            # remove pinned object from sorted ids
            sorted_ids.pop(pinned_index)
            # iterate up in sorted_ids from 'pinned'
            prev_max = self.get_bbox_coord(pinned, "max", axis)
            for node_id in sorted_ids[pinned_index:]:
                old_min, old_max = self.get_bbox_bounds(node_id, axis)
                new_min = prev_max + offset
                new_max = old_max + (new_min - old_min)
                # update cache for previous edge
                prev_max = new_max
                # store absolute delta per node_id
                deltas[node_id] = new_min - old_min
            # iterate down in sorted_ids from 'pinned'
            prev_min = self.get_bbox_coord(pinned, "min", axis)
            for node_id in reversed(sorted_ids[:pinned_index]):
                old_min, old_max = self.get_bbox_bounds(node_id, axis)
                new_max = prev_min - offset
                new_min = new_max - (old_max - old_min)
                # update cache for previous edge
                prev_min = new_min
                # store absolute delta per node_id
                deltas[node_id] = new_max - old_max

    def set_distribute_gap_area(self, tdict, axis, sorted_ids, deltas):
        """Compute deltas for gaps filling anchor bounds."""
        # pylint: disable=too-many-locals
        # check number of objects
        if len(sorted_ids) < 2:
            msg = "'Distribute' requires 2 or more objects."
            return msg
        # get target bounds
        target_min, target_max = tdict['anchor_bounds']
        # compute gap size between objects relative to target area
        target_size = target_max - target_min
        bboxes_size = self.get_bboxes_size_sum(sorted_ids, axis)
        offset = (target_size - bboxes_size) / (len(sorted_ids) - 1)
        # remove pinned objects from sorted ids
        first = sorted_ids.pop(0)
        last = sorted_ids.pop(-1)
        # get pinned objects bounds
        first_min, first_max = self.get_bbox_bounds(first, axis)
        last_max = self.get_bbox_coord(last, "max", axis)
        # store delta for pinned first, last objects
        deltas[first] = target_min - first_min
        deltas[last] = target_max - last_max
        # cache max from first object
        prev_max = target_min + (first_max - first_min)
        # compute delta for each id in sorted_ids
        for node_id in sorted_ids:
            old_min, old_max = self.get_bbox_bounds(node_id, axis)
            new_min = prev_max + offset
            new_max = old_max + (new_min - old_min)
            # update cache for previous edge
            prev_max = new_max
            # store absolute delta per node_id
            deltas[node_id] = new_min - old_min

    def set_distribute_gap_deltas(self, tdict, axis, sorted_ids):
        """Add delta per axis and object for gap to target dict."""
        # group types of anchors (targets)
        areas = ['selection', 'page', 'drawing']
        pins = ['last', 'first', 'biggest', 'smallest', 'top', 'bottom']
        deltas = dict()
        # compute deltas per object based on anchor
        if tdict['anchor'] in areas:
            self.set_distribute_gap_area(tdict, axis, sorted_ids, deltas)
        elif tdict['anchor'] in pins and tdict['anchor_ref'] == 'pinned':
            self.set_distribute_gap_pinned(tdict, axis, sorted_ids, deltas)
        elif tdict['anchor'] in pins and tdict['anchor_ref'] == 'extents':
            self.set_distribute_gap_object(tdict, axis, sorted_ids, deltas)
        # update deltas in transform dict
        if len(deltas):
            tdict['deltas'] = dict(deltas)

    # ----- transform helper methods: distribute refs

    def get_distribute_ref_object(self, tdict, axis, sorted_ids):
        """Return start point and offset for object bounds."""
        # pylint: disable=too-many-locals
        ref = tdict['object_ref']
        pinned = start = offset = None
        # check number of objects
        if len(sorted_ids) < 3:
            msg = "'Distribute' requires 3 or more objects."
            return msg
        # pinned object
        if len(tdict['exclude']):
            pinned = tdict['exclude'][0]
        if pinned is not None:
            pinned_index = sorted_ids.index(pinned)
            sorted_ids.pop(pinned_index)
            # selection ref bounds without pinned object
            first_ref = self.get_bbox_coord(sorted_ids[0], ref, axis)
            last_ref = self.get_bbox_coord(sorted_ids[-1], ref, axis)
            # selection bounds without pinned object
            first_min = self.get_bbox_coord(sorted_ids[0], "min", axis)
            last_max = self.get_bbox_coord(sorted_ids[-1], "max", axis)
            # target bounds (pinned object)
            pinned_min, pinned_max = self.get_bbox_bounds(pinned, axis)
            # compute offset between object refpoints
            start_ref = pinned_min + (first_ref - first_min)
            end_ref = pinned_max - (last_max - last_ref)
            offset = (end_ref - start_ref) / (len(sorted_ids) - 1)
            # compute new start
            start = start_ref
        return start, offset

    def get_distribute_ref_pinned(self, tdict, axis, sorted_ids):
        """Return start point and offset for pinned object."""
        # pylint: enable=too-many-locals
        ref = tdict['object_ref']
        pinned = start = offset = None
        # check number of objects
        if len(sorted_ids) < 2:
            msg = "'Distribute' requires 2 or more objects."
            return msg
        # selection ref bounds
        first_ref = self.get_bbox_coord(sorted_ids[0], ref, axis)
        last_ref = self.get_bbox_coord(sorted_ids[-1], ref, axis)
        # pinned object
        if len(tdict['exclude']):
            pinned = tdict['exclude'][0]
        if pinned is not None:
            pinned_index = sorted_ids.index(pinned)
            # pinned object ref
            pinned_ref = self.get_bbox_coord(pinned, ref, axis)
            # compute offset between object refpoints
            start_ref = first_ref
            end_ref = last_ref
            offset = (end_ref - start_ref) / (len(sorted_ids) - 1)
            # compute new start
            start = pinned_ref - (pinned_index * offset)
        return start, offset

    def get_distribute_ref_area(self, tdict, axis, sorted_ids):
        """Return start point and offset for area bounds."""
        # pylint: disable=too-many-locals
        ref = tdict['object_ref']
        # check number of objects
        if len(sorted_ids) < 2:
            msg = "'Distribute' requires 2 or more objects."
            return msg
        # selection ref bounds
        first_ref = self.get_bbox_coord(sorted_ids[0], ref, axis)
        last_ref = self.get_bbox_coord(sorted_ids[-1], ref, axis)
        # selection bounds
        first_min = self.get_bbox_coord(sorted_ids[0], "min", axis)
        last_max = self.get_bbox_coord(sorted_ids[-1], "max", axis)
        # target bounds
        target_min, target_max = tdict['anchor_bounds']
        # compute offset between object refpoints
        start_ref = target_min + (first_ref - first_min)
        end_ref = target_max - (last_max - last_ref)
        offset = (end_ref - start_ref) / (len(sorted_ids) - 1)
        # compute new start
        start = start_ref
        return start, offset

    def set_distribute_ref_deltas(self, tdict, axis, sorted_ids):
        """Add delta per axis and object for bbox ref to target dict."""
        ref = tdict['object_ref']
        # group types of anchors (targets)
        areas = ['selection', 'page', 'drawing']
        pins = ['last', 'first', 'biggest', 'smallest', 'top', 'bottom']
        deltas = dict()
        params = [None, None]
        if tdict['anchor'] in areas:
            params = self.get_distribute_ref_area(tdict, axis, sorted_ids)
        elif tdict['anchor'] in pins and tdict['anchor_ref'] == 'pinned':
            params = self.get_distribute_ref_pinned(tdict, axis, sorted_ids)
        elif tdict['anchor'] in pins and tdict['anchor_ref'] == 'extents':
            params = self.get_distribute_ref_object(tdict, axis, sorted_ids)
        start, offset = params
        if start is not None and offset is not None:
            # compute deltas based on start, offset for sorted_ids
            for j, node_id in enumerate(sorted_ids):
                old = self.get_bbox_coord(node_id, ref, axis)
                new = start + (j * offset)
                deltas[node_id] = new - old
            # update deltas in transform dict
            tdict['deltas'] = dict(deltas)

    # ----- transforms

    def stretch(self, id_list, params):
        """Apply stretch transformation to objects in id_list."""
        # pylint: disable=arguments-differ
        for node_id in id_list:
            # merge stretch params per node_id
            node_params = {}
            for key in params.keys():
                if node_id in params[key]:
                    node_params[key] = params[key][node_id]
            if len(node_params):
                if all(coord is None for _, coord in node_params.values()):
                    continue  # nothing to do, try next node_id
                factor = [1.0, 1.0]
                anchor = [0.0, 0.0]
                for axis, (ratio, coord) in node_params.items():
                    i = get_axis_index(axis)
                    if coord is not None:
                        factor[i] = ratio
                        anchor[i] = coord
                node = self.get_object_by_id(node_id)
                # transform to root, scale, transform back to node
                mat_stretch = [[factor[0], 0.0, 0.0],
                               [0.0, factor[1], 0.0]]
                mat = tfo.get_mat_composed_in_root(node, anchor, mat_stretch)
                self.transform(mat, node, update=False)

    def translate(self, id_list, params):
        """Apply translate transformation to objects in id_list."""
        for node_id in id_list:
            # extract matrix params
            tx = params[AXIS_X_KEY].get(node_id, 0.0)
            ty = params[AXIS_Y_KEY].get(node_id, 0.0)
            # transform
            anchor = None
            if tx or ty:
                node = self.get_object_by_id(node_id)
                # transform to root, translate, transform back to node
                mat_translate = [[1.0, 0.0, tx],
                                 [0.0, 1.0, ty]]
                mat = tfo.get_mat_composed_in_root(node, anchor, mat_translate)
                self.transform(mat, node, update=False)

    # ----- arrange objects

    def remove_overlaps(self, id_list):
        """Main entry for 'Remove Overlaps' notebook page."""
        # notify about unfinished features
        if not DEBUG:
            # notify about unfinished features
            inkex.debug("*** 'Overlaps' not yet implemented ***\n")
            return self.demo()
        inkex.debug('remove_overlaps: {}'.format(id_list))

    def rearrange_objects(self, id_list):
        """Main entry for 'Re-arrange Objects' notebook page."""
        # notify about unfinished features
        if not DEBUG:
            # notify about unfinished features
            inkex.debug("*** 'Re-arrange' not yet implemented ***\n")
            return self.demo()
        inkex.debug('rearrange_objects: {}'.format(id_list))

    def stretch_objects(self, id_list, action):
        """Main entry for 'Stretch Objects' notebook page."""
        # pylint: disable=too-many-locals
        # check coordinate system, switch y refpoints as needed
        self.flip_vertical_refs(action)
        # set options used in transform_objects.py for scaling
        self.options.sx_unit = "uu"
        self.options.sy_unit = "uu"
        self.options.scale_proportionally = False
        self.options.scale_position_only = False
        # process all objects per axis
        params = get_params_default()
        for axis in params.keys():
            # copy original id_list
            axis_ids = list(id_list)
            # check anchor
            anchor = getattr(self.options, action+'_'+axis+'_anchor')
            if anchor == 'none':
                continue
            # get anchor and object refpoint
            anchor_ref = getattr(self.options, action+'_'+axis+'_anchor_ref')
            object_ref = getattr(self.options, action+'_'+axis+'_object_ref')
            # exclude text/non-text axis_ids
            object_ref, skipped = self.check_text_objects(object_ref, axis_ids)
            for node_id in skipped:
                if node_id in axis_ids:
                    axis_ids.remove(node_id)
            # update bbox cache (without skipped objects)
            self.update_bboxes(axis_ids)
            # get anchor bounds and pinned anchors
            anchor_bounds, pinned = self.get_anchor_bounds(
                axis, anchor, axis_ids)
            # exclude anchors
            for node_id in pinned:
                if node_id in axis_ids:
                    if self.options.highlight_anchor:
                        self.highlight(node_id, get_highlight_color(axis))
                    axis_ids.remove(node_id)
            # check number of objects
            if not len(axis_ids):
                inkex.debug('No objects to {}.'.format(action))
            else:
                # iterate objects in axis_ids
                tdict = {'anchor': anchor,
                         'anchor_bounds': anchor_bounds,
                         'anchor_ref': anchor_ref,
                         'object_ref': object_ref}
                for node_id in axis_ids:
                    bounds = self.get_bbox_bounds(node_id, axis)
                    node_params = get_stretch_object_params(tdict, bounds)
                    params[axis][node_id] = node_params
        # process all objects for transform params
        objects = get_params_objects_set(params)
        self.stretch(objects, params)

    def distribute_objects(self, id_list, action):
        """Main entry for 'Distribute Objects' notebook page."""
        # pylint: disable=too-many-locals
        # pylint: disable=too-many-branches
        # check coordinate system, switch y refpoints as needed
        self.flip_vertical_refs(action)
        # process all objects per axis
        params = get_params_default()
        for axis in params.keys():
            # copy original id_list
            axis_ids = list(id_list)
            # check anchor
            anchor = getattr(self.options, action+'_'+axis+'_anchor')
            if anchor == 'none':
                continue
            # get anchor and object refpoint
            anchor_ref = getattr(self.options, action+'_'+axis+'_anchor_ref')
            object_ref = getattr(self.options, action+'_'+axis+'_object_ref')
            # exclude text/non-text axis_ids
            object_ref, skipped = self.check_text_objects(object_ref, axis_ids)
            skip_all = anchor in ['page', 'drawing'] or anchor_ref == 'pinned'
            # check skipped objects
            if skip_all:
                for node_id in skipped:
                    if node_id in axis_ids:
                        axis_ids.remove(node_id)
            # update bbox cache (without skipped objects)
            self.update_bboxes(axis_ids)
            # get anchor bounds and pinned anchors
            anchor_bounds, pinned = self.get_anchor_bounds(
                axis, anchor, axis_ids)
            # check skipped objects
            if not skip_all:
                for node_id in skipped:
                    # drop unused (not pinned) skipped objects
                    if node_id not in pinned:
                        if node_id in axis_ids:
                            axis_ids.remove(node_id)
            # highlight anchors
            if self.options.highlight_anchor:
                for node_id in pinned:
                    if node_id in axis_ids:
                        self.highlight(node_id, get_highlight_color(axis))
            # iterate objects in axis_ids
            tdict = {'anchor': anchor,
                     'anchor_bounds': anchor_bounds,
                     'anchor_ref': anchor_ref,
                     'object_ref': object_ref,
                     'exclude': pinned,
                     'deltas': dict()}
            # check number of objects
            ret = None
            if not len(axis_ids):
                ret = 'No objects to {}.'.format(action)
            elif object_ref == 'gaps':
                sorted_ids = [node_id for node_id
                              in self.get_sorted_ids(sortby='mid')[axis]
                              if node_id in axis_ids]
                ret = self.set_distribute_gap_deltas(tdict, axis, sorted_ids)
            elif object_ref in ['min', 'mid', 'max']:
                sorted_ids = [node_id for node_id
                              in self.get_sorted_ids(sortby=object_ref)[axis]
                              if node_id in axis_ids]
                ret = self.set_distribute_ref_deltas(tdict, axis, sorted_ids)
            else:
                ret = "Unsupported '{}' object refpoint.".format(action)
            if ret is not None:
                inkex.debug(ret)
            else:
                params[axis] = tdict['deltas']
        # process all objects for transform params
        objects = get_params_objects_set(params)
        self.translate(objects, params)

    def align_objects(self, id_list, action):
        """Main entry for 'Align Objects' notebook page."""
        # pylint: disable=too-many-locals
        # pylint: disable=too-many-branches
        # pylint: disable=too-many-statements
        # check coordinate system, switch y refpoints as needed
        self.flip_vertical_refs(action)
        # process all objects per axis
        params = get_params_default()
        for axis in params.keys():
            # copy original id_list
            axis_ids = list(id_list)
            # check anchor
            anchor = getattr(self.options, action+'_'+axis+'_anchor')
            if anchor == 'none':
                continue
            # get anchor and object refpoint
            anchor_ref = getattr(self.options, action+'_'+axis+'_anchor_ref')
            object_ref = getattr(self.options, action+'_'+axis+'_object_ref')
            align_context = getattr(self.options, action+'_'+axis+'_context')
            # exclude text/non-text axis_ids
            object_ref, skipped = self.check_text_objects(object_ref, axis_ids)
            # update bbox cache
            self.update_bboxes(axis_ids)
            # get anchor bounds and pinned anchors
            anchor_bounds, pinned = self.get_anchor_bounds(
                axis, anchor, axis_ids)
            # exclude anchors
            for node_id in pinned:
                if node_id in axis_ids:
                    axis_ids.remove(node_id)
                    # highlight anchors
                    if self.options.highlight_anchor:
                        self.highlight(node_id, get_highlight_color(axis))
            # skipped objects when aligning text anchors
            for node_id in skipped:
                if node_id in axis_ids:
                    axis_ids.remove(node_id)
            # align anchor axis coordinate
            anchor_coord = ref_coord(anchor_ref, anchor_bounds)
            # check number and context of objects
            msg = None
            if not len(axis_ids):
                msg = "No objects to {}.".format(action)
            elif align_context == 'parent':
                # parent group params
                if len(axis_ids) == 1:
                    node_id = axis_ids.pop(0)
                    node = self.get_object_by_id(node_id)
                    parent = node.getparent()
                    if parent is not None:
                        if not is_layer(parent):
                            object_bounds = self.get_bbox_bounds(node_id, axis)
                            object_coord = ref_coord(object_ref, object_bounds)
                            delta = anchor_coord - object_coord
                            params[axis][parent.get('id')] = delta
                        else:  # if is_layer(parent):
                            msg = "Parent group is a layer."
                else:  # if len(axis_ids) > 1:
                    msg = ("Context 'Parent group' requires 2 parameters: "
                           "the anchor, and one selected group member.")
            else:  # if align_context != 'parent':
                # group params
                if align_context == 'as_group':
                    object_bounds = self.get_bboxes_bounds(axis_ids, axis)
                    object_coord = ref_coord(object_ref, object_bounds)
                    delta = anchor_coord - object_coord
                # iterate objects in axis_ids
                for node_id in axis_ids:
                    # node params
                    if align_context != 'as_group':
                        object_bounds = self.get_bbox_bounds(node_id, axis)
                        object_coord = ref_coord(object_ref, object_bounds)
                        delta = anchor_coord - object_coord
                    params[axis][node_id] = delta
            if msg is not None:
                inkex.debug(msg)
        # process all objects for transform params
        objects = get_params_objects_set(params)
        self.translate(objects, params)

    # ----- second-level dispatcher

    def recurse_selection(self, node, id_list, level=0, current=0):
        """Recursively process selection, add checked elements to id list."""
        current += 1
        if not level or current <= level:
            if is_group(node):
                for child in node.iterchildren("*"):
                    id_list = self.recurse_selection(
                        child, id_list, level, current)
            else:
                id_list.append(node.get('id'))
        else:
            id_list.append(node.get('id'))
        return id_list

    def get_group_children(self, id_list):
        """Return id list of all children in selected groups."""
        child_ids = []
        # level = 0: unlimited recursion into groups
        # level = 1: process top-level groups only
        level = 0
        for node_id in id_list:
            node = self.get_object_by_id(node_id)
            self.recurse_selection(node, child_ids, level)
        return child_ids

    def get_current_layer_children(self):
        """Return id list of objects in current layer."""
        child_ids = []
        layer = self.current_layer
        if layer is not None:
            for child in layer.iterchildren("*"):
                if not (is_metadata(child) or
                        is_style(child) or
                        is_script(child) or
                        is_namedview(child) or
                        is_defs(child) or
                        is_layer(child)):
                    child_ids.append(child.get('id'))
        return child_ids

    def get_object_ids(self, action):
        """Return list of relevant object ids for chosen action."""
        id_list = None
        objects = getattr(self.options, action+'_objects')
        if objects == 'selection':
            id_list = self.selected.keys()
        elif objects == 'group':
            id_list = self.get_group_children(self.selected.keys())
        elif objects == 'layer':
            id_list = self.get_current_layer_children()
        return id_list

    def check_options(self, action=None):
        """Check options for unsupported combinations."""
        # pylint: disable=no-self-use
        # pylint: disable=unused-argument
        msgs = []
        # add checks for action here
        if not len(msgs):
            return True
        else:
            for msg in msgs:
                inkex.debug(msg)

    def config_preset_action(self):
        """Configure options for a preset action."""
        action = None
        preset = self.options.preset_action
        objects = self.options.preset_objects
        if preset == 'align_page_mid_group':
            action = 'align'
            setattr(self.options, action+'_objects', objects)
            setattr(self.options, action+'_x_anchor', 'page')
            setattr(self.options, action+'_x_anchor_ref', 'mid')
            setattr(self.options, action+'_x_object_ref', 'mid')
            setattr(self.options, action+'_x_as_group', True)
            setattr(self.options, action+'_y_anchor', 'page')
            setattr(self.options, action+'_y_anchor_ref', 'mid')
            setattr(self.options, action+'_y_object_ref', 'mid')
            setattr(self.options, action+'_y_as_group', True)
        elif preset == 'align_page_xmid':
            action = 'align'
            setattr(self.options, action+'_objects', objects)
            setattr(self.options, action+'_x_anchor', 'page')
            setattr(self.options, action+'_x_anchor_ref', 'mid')
            setattr(self.options, action+'_x_object_ref', 'mid')
            setattr(self.options, action+'_x_as_group', False)
            setattr(self.options, action+'_y_anchor', 'none')
            setattr(self.options, action+'_y_as_group', False)
        elif preset == 'align_page_ymid':
            action = 'align'
            setattr(self.options, action+'_objects', objects)
            setattr(self.options, action+'_x_anchor', 'none')
            setattr(self.options, action+'_x_as_group', False)
            setattr(self.options, action+'_y_anchor', 'page')
            setattr(self.options, action+'_y_anchor_ref', 'mid')
            setattr(self.options, action+'_y_object_ref', 'mid')
            setattr(self.options, action+'_y_as_group', False)
        return action

    def transform_objects(self):
        """Main entry for 'Arrange Objects' extension."""
        action = None
        id_list = None
        # common options (without GUI)
        self.options.apply_separately = True
        self.options.apply_to_copy = False
        if tab_name(self.options.tab) == 'presets_tab':
            # preset action
            action = self.config_preset_action()
        else:
            # custom action
            action_tab = tab_name(self.options.transform_notebook)
            action = strip_end(action_tab, '_tab')
        # check for unsupported combinations of options
        if not self.check_options(action):
            return
        # action
        if action is not None:
            # list of object ids
            id_list = self.get_object_ids(action)
            if id_list is not None and len(id_list):
                if action == 'align':
                    self.align_objects(id_list, action)
                elif action == 'distribute':
                    self.distribute_objects(id_list, action)
                elif action == 'stretch':
                    self.stretch_objects(id_list, action)
                elif action == 'rearrange':
                    self.rearrange_objects(id_list)
                elif action == 'overlaps':
                    self.remove_overlaps(id_list)
            else:
                inkex.debug('No objects to {}.'.format(action))
        else:
            inkex.debug("No action selected to arrange objects.")

    # ----- main

    def effect(self):
        """Main entry point to process current document."""
        func = getattr(self, self.options.extension, None)
        if func is not None:
            return func()
        else:
            inkex.debug('{} not found.'.format(self.options.extension))


if __name__ == '__main__':
    ME = TransformObjects()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
